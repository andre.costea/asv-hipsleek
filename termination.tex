\newcommand{\Term}[1]{\ensuremath{\code{Term} ~ #1}}
\newcommand{\Loop}{\code{Loop}}
\newcommand{\MayLoop}{\code{MayLoop}}
\newcommand{\RC}[2]{\ensuremath{\code{RC}\langle #1, #2 \rangle}}
\newcommand{\RSplit}{\ensuremath{\blacktriangleright}}
\newcommand{\entailR}[3]{\ensuremath{#1 \vdash #2 \RSplit #3}}
\newcommand{\splitR}[2]{\ensuremath{#1 \RSplit #2}}
\newcommand{\defineas}{\triangleq}

The problems of proving program \emph{termination} and \emph{non-termination} are orthogonal. While termination can be encoded as a \emph{liveness} property, non-termination is considered as a \emph{safety} property. In this section, we will introduce a \emph{unified} component in the HIP system for reasoning \emph{both} termination and non-termination of imperative programs at the same time. The component, whose structure was represented in \cite{LeTC:TACAS17}, was implemented based on two main techniques:

\begin{itemize}
  \item A resource-based specification logic and an automated verification system for specifying and verifying termination and non-termination properties of programs \cite{LeGHC:ICFEM14}.
  \item A modular inference system for automatically inferring termination and non-termination specification of the programs. The system employs an abductive inference technique with second-order specification to derive a summary of terminating and non-terminating behaviors for each method in the program \cite{LeQC:PLDI15}.
\end{itemize}

\subsection{A Verification System for Termination and Non-termination}
\label{sec:term_logic}
We propose three primitive temporal predicates {\Term{X}} with the ranking function $X$, {\Loop}, and {\MayLoop} to specify the program termination, definite non-termination, and possibly non-termination, respectively. We then extend the core specification language of HIP (cf. Section \ref{sec:spec}) to these temporal predicates and integrate their reasoning into HIP's forward verification system (cf. Section \ref{sec:verif_rules}) to specify and verify program termination and non-termination. As a result, we can utilize the rich specification language and the available verification infrastructure for reasoning about terminating and non-terminating behaviors of various programs.

In order to do that, we propose a resource-based logic with the unified predicate \RC{l}{u} denoting the lower bound $l$ and the upper bound $u$ of available resource capacity for program execution, given that $0\,{\leq}\,l\,{\leq}\,u$. In this logic, each method requires an initial resource for its execution, specified by a predicate \RC{l}{u} in its precondition, and the verification system then statically monitors the consumption of this resource to guarantee that it is always sufficient for the execution. To keep track of the consumed and remaining resources within the same entailment checking of the existing verification system, we design an entailment checking with \emph{frame} for resource reasoning via the resource splitting operation \RSplit, which is similar to the heap separating conjunction in separation logic. In particular, the splitting resource assertion $\RC{l_1}{u_1} \RSplit \RC{l_2}{u_2}$ holds for a resource indicated by $\RC{l}{u}$ if and only if that resource can be split into two resource fragments, on which $\RC{l_1}{u_1}$ and $\RC{l_2}{u_2}$ hold respectively. In this case, we say that the entailment $\entailR{\RC{l}{u}}{\RC{l_1}{u_1}}{\RC{l_2}{u_2}}$ is valid. Intuitively, this entailment encodes a resource consumption of $\RC{l_1}{u_1}$ on the original resource $\RC{l}{u}$ with the remaining resource $\RC{l_2}{u_2}$. The entailment checking ensures that $\RC{l}{u}$ is sufficient for $\RC{l_1}{u_1}$ to be consumed.
During program verification, the resource entailment checking mainly happens at the method calls, so that we can check if the current resource of the caller satisfies the resource requirement of the callee. In addition, when the method returns, we check if the lower bound of the remaining resource is $0$ in order to ensure that the initial given resource satisfies the minimum resource requirement of the method.

Specifically for the termination and non-termination reasoning, we model the three temporal predicates as follows:

\[
\begin{array}{rll}
\Term{X} & \defineas & \RC{0}{\rho(X)} \\
\Loop & \defineas & \RC{\infty}{\infty} \\
\MayLoop & \defineas & \RC{0}{\infty}
\end{array}
\]
where $\rho(X)$ is an order-embedding of the ranking function $X$ into naturals. Intuitively, a terminating method indicated by {\Term{X}} must have a finite upper bound of the resource to execute. On the other hand, a definitely non-terminating method indicated by {\Loop} requires an infinite lower bound of the resource, so that it has sufficient resource to execute forever. Lastly, a possible non-terminating method indicated by {\MayLoop} does not have any specific requirement, so that its required resource ranges from $0$ to $\infty$. The resource entailment checking rules of these specific predicates are:
\[
\begin{array}{ll}
\begin{array}{lll}
\MayLoop & \vdash & \splitR{\MayLoop}{\MayLoop} \\
\MayLoop & \vdash & \splitR{\Loop}{\MayLoop} \\
\MayLoop & \vdash & \splitR{\Term{X}}{\MayLoop} \\
\Loop & \vdash & \splitR{\MayLoop}{\Loop}
\end{array}
&
\begin{array}{lll}
\Loop & \vdash & \splitR{\Loop}{\MayLoop} \\
\Loop & \vdash & \splitR{\Term{X}}{\Loop} \\
\multicolumn{3}{c}{
\begin{array}{c}
Y < X \\
\hline
\entailR{\Term{X}}{\Term{Y}}{\Term{X}}\\
\end{array}}
\end{array}
\end{array}
\]
These rules follow the general resource entailment checking for monitoring the execution length of the method. A program state with {\MayLoop} or {\Loop} accepts any other resource requirement by their $\infty$ upper bound. However, the $\infty$ lower bound of {\Loop} is only consumed by the other {\Loop} to become a {\MayLoop}. A program state with {\Term{X}} only accepts another program state with a finite resource requirement {\Term{Y}} which is smaller than $X$. Here, the frame {\Term{X}} is an over-approximation of the actual remaining resource. This over-approximation is safe since its upper bound is finite and does not exceed the original resource requirement. The other entailments, such as \entailR{\Term{X}}{\Loop}{\_} and \entailR{\Term{X}}{\MayLoop}{\_}, are invalid since they indicates that the caller's resource does not meet the callee's resource requirement. Finally, the termination and non-termination reasoning follows the resource reasoning by requiring that {\Loop} with a non-zero lower bound cannot be observed in program states when the method returns.

\subsection{A Termination and Non-termination Specification Inference}
\label{sec:term_infer}
In the next step, we aim to automatically infer the termination specifications with {\Term{X}}, {\Loop}, and {\MayLoop} associated with their pre-conditions. The general idea is that given a pre-condition (\code{true} initially), we attempt to prove if under this pre-condition, the method terminates (by determining if the pre-condition is a base case or otherwise synthesizing a valid ranking function) or does not terminate (by proving that the method exit is unreachable). If these attempts both fail, we apply an \emph{abductive inference} on the failure of the non-termination proof to derive a new pre-condition in which the base case is unreachable in the \emph{next step} of the execution. Using \emph{case analysis}, we refine the currently considered pre-condition into two distinct cases: the newly derived abductive pre-condition and its complement. We then repeat the inference process with these two new pre-conditions. The refinement iteration stops when we can determine the termination (\Term{X}) or non-termination (\Loop) status of all pre-conditions, so that there is no new unknown pre-conditions derived or when the number of iterations reaches a preset number, in which all unknown pre-conditions are marked as {\MayLoop}.

We do not implement the termination inference algorithm from scratch but utilize the existing HIP verification system. We first extend its specification logic for termination and non-termination reasoning (cf. Section \ref{sec:term_logic}) to an unknown temporal predicate; thus, the other existing temporal predicates {\Term{X}}, {\Loop}, and {\MayLoop} are considered as known predicates and are solutions of the new unknown predicate. Then, we annotate the analyzed method with a unknown specification and use the same termination and non-termination reasoning to collect a set of relational assumptions over the unknown predicates in this specification. Finally, we solve these relational assumptions with the above refinement iteration process to obtain the known solutions of the unknown predicates. By not analyzing the source program directly but through the verification against unknown specifications, we consider our algorithm as a \emph{second-order} inference technique.
