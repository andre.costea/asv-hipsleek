\subsection{Formalisms for Shape Analysis of Data Structures}

Many formalisms have been  proposed for analyzing the shape of data
structures in imperative programs. One well-known work is the Pointer
Assertion Logic \cite{MollerS01} by Moeller and Schwartzbach, which is a
highly expressive mechanism to describe invariants of graph types
\cite{KlarlundS93}. The Pointer Assertion Logic Engine (PALE) uses Monadic
Second-Order Logic over Strings and Trees as the underlying logic and the
tool MONA \cite{HenriksenJJKPRS95} as the prover. PALE invariants are not
designed to handle arithmetic, hence it is not possible to encode
height-balanced trees in PALE. Moreover, PALE is unsound in
handling procedure calls \cite{MollerS01}, whereas we would like to have a
sound verifier. 

Harwood et al. \cite{HarwoodCW08} describe a UTP theory for
objects and sharing in languages like Java or C++. Their work focuses on a
denotational model meant to provide a semantical foundation for
refinement-based reasoning or Hoare-style axiomatic reasoning. Our work
focuses more on practical verification for heap-manipulating programs.

In an object-oriented setting, the Dafny language \cite{Leino10} uses
dynamic frames in its specifications. The term frame refers to a set of
memory locations, and an expression denoting a frame is dynamic in the
sense that as the program executes, the set of locations denoted by the
frame can change. A dynamic frame is thus denoted by a set-valued
expression (in particular, a set of object references), and this set is
idiomatically stored in a field. Methods in Dafny use modifies and reads
clauses, which frame the modifications of methods and dependencies of
functions. By comparison, separation logic provides a reasoning logic that
hides the explicit representation of dynamic frames.

For shape inference, Sagiv et al. \cite{SagivRW99} present a parameterized
framework, called TVLA, using 3-valued logic formulae and abstract
interpretation. Based on the properties expected of data structures,
%% programmers
the users may either learn or 
supply a set of predicates to the framework which are then
used to analyse that certain shape invariants are maintained. 

However, most of these techniques are focused on analysing shape
invariants, and do not attempt to track the size and bag properties of
complex data structures. An exception is the quantitative shape analysis %% of
%% Rugina
\cite{Rugina04} where a data flow analysis is proposed to compute
quantitative information for programs with destructive updates. By tracking
unique points-to reference and its height property, their algorithm is able
to handle AVL-like tree structures. Even then, the author acknowledges the
lack of a general specification mechanism for handling arbitrary shape/size
properties.

\subsection{Reasoning with Inductive Heap Predicates}

In separation logic, the first automated procedure to handle inductive heap predicates was
proposed by Berdine et al. \cite{BerdineCO04, BerdineCO05}. It reasons %% with
about the recursive structure of an inductive heap predicate by folding/unfolding
the predicate against its definition. However, this work is hardwired to
work for only \code{lseg} and \code{tree} predicates. Furthermore, it only
performs predicate unfolding in the consequent of an entailment which may
miss bindings on free variables. Compared to \cite{BerdineCO04,
  BerdineCO05}, our unfold/fold mechanism is general, automatic and
terminates for heap entailment checking.

Besides \cite{BerdineCO04, BerdineCO05}, to date, there are also various
approaches that hard-wire or pre-define inductive heap predicates to model
specific types of the linked list and the tree data structures, such as the
works of Piskac et al. \cite{PiskacWZ13}, Bozga et al. \cite{BozgaIP10},
and Perez et al. \cite{PerezR11, PerezR13}. These works provide specific
syntax and semantics for predicates in advance so that they can derive
efficient techniques to handle these predicates when proving entailments.
Since the invented techniques are tied to certain types of pre-defined
predicates, they might not be automatically extended to reason about other
inductive heap predicates.

A more general approach is to consider classes of inductive heap predicates
satisfying certain syntactic or semantic restrictions, such as predicates
with a bounded tree width property by Iosif et al.
\cite{IosifRS13,IosifRV14} or predicates describing variants of the linked
list data structure by Enea et al. \cite{EneaLSV14}. These authors propose
to prove entailments by translating separation logic entailments into
equivalent formulas in theories of automata or graphs. Thereby, they can
employ developed proof techniques in automata and graph theories to prove
the translated formulas, and conclude about the validity of the original
separation logic entailments. Nevertheless, inductive heap predicates in
this approach might not be able to represent sophisticated properties of
data structures such as arithmetic constraints about their size or elements'
content. These constraints are not directly supported by the considered
external theories of graphs and automata.

To resolve the above expressiveness limitation, in this work, we consider
more general classes of user-defined inductive heap predicates, i.e.
predicates which can be arbitrarily defined by users of verification and
analysis systems and propose to prove entailments by using sequent-based
proof systems with the folding/unfolding and predicate matching/removing
mechanisms. Similar approaches are also utilized by Qiu et al.
\cite{QiuGSM13, PekQM14}, and Enea et al. \cite{EneaSW15}. However, there
is a limitation in all of these works and ours: the proof derivation to unfold
and match predicates can be infinite. Therefore, we sometimes require users
to provide supplementing lemmas assisting the proof system to compose,
decompose or reorganize inductive heap predicates without the need of
unfolding.

In recent works, Brotherston et al. \cite{BrotherstonDP11}, Chu et al.
\cite{ChuJT15}, and Ta et al. \cite{TaLKC16, TaLKC19} propose to overcome
the above limitation through %%by using cyclic or induction proof
inductive inference
 systems. Particularly, %% In these
%% systems,
the infinite unfolding sequences of inductive heap predicates would
be avoided by the detection of proof cycles \cite{BrotherstonDP11} or the
application of induction hypotheses \cite{ChuJT15, TaLKC16, TaLKC19}.
Finally, in \cite{TaLKC18}, Ta et al. present a technique
to automatically
synthesize lemmas by combining induction proof and constraint solving, to
assist proving entailments. Certainly, this work can free the users from
the labor task of manually inspecting and providing necessary lemmas, which
are needed in the proofs.

\subsection{Beyond Shape: Size, Set, and Bag Properties}

In another direction of research, size properties are mostly explored for
declarative languages \cite{HughesPS96, XiP99, ChinK00} as the immutability
property makes their data structures easier to analyse statically. Size
analysis is also extended to object-based programs \cite{ChinKQPN05} but is
restricted to tracking either size-immutable objects that can be aliased
and size-mutable objects that are unaliased, with no support for complex
shapes.

The Applied Type System (ATS) \cite{ChenX05} is proposed for combining
programs with proofs. In ATS, dependent types for capturing program
invariants are extremely expressive and can capture many program properties
with the help of accompanying proofs. Using linear logic, ATS may also
handle mutable data structures with sharing in a precise manner. However,
users must supply all expected properties, and precisely state where they
are to be applied, with ATS playing the role of a proof-checker. In
comparison, we use a more limited class of constraint for shape, size and
bag analysis but support automated modular verification.

On the other hand, set-based analysis is proposed to verify data structure
consistency properties in the work of Kuncak et al. \cite{KuncakNR05},
where a decision procedure is given for a first order theory that combines
set and Presburger arithmetic. This result may be used to build a
specialised mixed constraint solver but it currently has high algorithmic
complexity. Lahiri and Qadeer \cite{LahiriQ06} report an intra-procedural
reachability analysis for well-founded linked lists using first-order
axiomatization. Reachability analysis is related to set/bag property that
we capture but implemented by transitive closure at the predicate level.

\subsection{Other Verification Systems in Non-Separation-Logic Settings}

Program verifiers that are based on Hoare-style logic have been around
longer than those based on separation logic. We describe some major efforts
in this direction.

ESC/Java \cite{FlanaganLLNSS02} is a verification system developed at
Compaq Systems Research Center, that aims to detect more errors than
traditional static checking tools, such as type checkers, but is not
designed to be a program verification system. The stated goals of ESC/Java
are scalability and usability. For that, it forgoes soundness for the
potential benefits of more automation and faster verification time. Hence,
ESC/Java suffers from both false negatives (programs that pass the check
may still contain errors that ESC/Java is designed to handle), and false
positives (programs flagged as erroneous are in fact correct programs). On
the contrary, our verifier is a sound program verifier as it does not
suffer from false negatives: if a program is verified, it is guaranteed to
meet its specifications for all possible program executions.

ESC/Java2 \cite{CokK04} is a continual effort of ESC/Java which adds
support for current versions of Java, and also verifies more JML
\cite{LeavensBR06} constructs. One significant addition is the support for
model fields and method calls within annotations \cite{Cok05}. Since
ESC/Java2 continues to use Simplify \cite{DetlefsNS05} as its underlying
theorem prover which does not support transitive closure operations, it may
have difficulties in verifying properties of heap-based data structures
that require reachability properties, such as collections of values stored
in container data structures.

Spec\# \cite{BarnettLS05} is a programming system developed at Microsoft
Research to verify C\# programms by utilizing its underlying verifier
Boogie \cite{BarnettLS05}. Spec\# adds constructs tailored to program
verification, such as pre- and post-conditions, frame conditions, non-null
types, model fields and object invariants. Spec\# also supports runtime
assertion checking and object invariants. In order to verify invariants,
Spec\# employs an ownership scheme that allows an object to own its
representation. This ownership scheme requires programmers to write special
commands to enforce unpacking and packing objects' invariants. In our
system, instead of using special fields in method contracts to indicate
whether an invariant should be enforced, users directly use predicates.
Hence, there is no need for explicitly packing and unpacking the objects in
the method body. Consequently, users are shielded from the details of the
verification methodology, which are largely irrelevant, from a user's point
of view.

Jahob \cite{KuncakLZR06} is a verification system that mainly focuses on
reasoning techniques for data structure verification that combines multiple
theorem provers to reason about expressive logical formulas. Jahob uses a
subset of the Isabelle/HOL \cite{NipkowPW02} language as its specification
language, and works on instantiatable data structures, as opposed to global
data structures used in its predecessor, Hob \cite{Lam07}. Like SPEC\# ,
Jahob supports ghost variables and specification assignments which places
onus on programmers to help in the verification process by providing
suitable instantiations of these specification variables.

EVE Proofs \cite{Tschannen09} is a verification system for Eiffel programs
by translating and conducting the verification with Boogie
\cite{BarnettLS05}. To infer the frame condition, this tool relies on an
automatic extraction of modifies clauses, which can be unsound. In
contrast, our approach does not have to infer frame conditions, courtesy to
the frame rule of separation logic \cite{reynolds2002separation}. Another
restriction of EVE Proofs regards the methodology for invariants, which has
to take into account that objects can temporarily violate the invariant,
but also that an object can call other objects while being in an
inconsistent state. As this is not considered at the moment, the current
implementation of invariants can introduce unsoundness in the system.

%% Now, we shall discuss the main differences of our verification system
%% compared
For a comparison
to the above non-separation-logic-based systems, our user-defined
predicates, which capture the properties to be analysed, can remove the
need for model fields and having object invariants tied to class/type
declarations. Regarding ghost specification variables, they are not
required since we provide support for automatically instantiating the
predicates' parameters. Furthermore, we utilize the unfold/fold mechanism
to handle recursive data structures. This obviates the need for specifying
transitive closure relations that are used by classical verifier, such as
Jahob, when tracking recursive properties. Lastly, as separation logic
employs local reasoning via a frame rule, our approach does not require a
separate modifies clause to be prescribed.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
