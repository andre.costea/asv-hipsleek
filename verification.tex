% \begin{verbatim}
% programming language
% specification language (multiple, imm, case)
% verification (modular, partial and total correctness, sequential &
% concurrent, loops)
% \end{verbatim}

We present an overview of our verification system HIP/SLEEK in
\figref{fig.overview}.
%
Its front-end is a Hoare-Floyd style forward-verifier, called HIP, which
analyzes C-like imperative programs against their specifications written in
pairs of pre- and post-conditions.
%
Its back-end is a logical prover, called SLEEK, which examines verification
conditions generated by HIP to decide whether the input programs satisfy
their specifications.
%
The details of this verification process are as follows:

\begin{itemize}
\item Firstly, the verifier HIP takes as input the C-like source code of
  the program to be verified along with its pre-/post-specifications;

\item Then, HIP verifies the input code against its specification in two
  phases.
  %
  (i) For each method definition, HIP assumes that the pre-condition holds,
  and it derives the strongest post-condition upon the method's body and
  checks whether the derived post-condition entails the declared
  post-condition (see \code{\rulen{METH-DEF}} in
  \subsecref{sec:verif_rules}).
  %
  (ii) For each method call, HIP checks whether the callee's pre-condition
  holds at the caller's site. If yes, it adds the corresponding
  post-condition to the caller's abstract state (see
  \code{\rulen{METH-CALL}} in \subsecref{sec:verif_rules}). See
  \figref{fig.prelim_verification} for a full list of the forward
  verification rules.

\item The verification rules often involve discharging proof obligations in
  the form of entailment checks, solved by the prover SLEEK (detailed in
  \secref{sec:entailment}).
  %
  SLEEK reasons about user-defined predicates, and, consequently, lemmas
  which relate the different predicates describing the same resource.

\item The specification language supported by HIP/SLEEK is rich (see
  \figref{fig.spec_syntax} for the full specification language), comprising
  not only shape properties, but also numerical ones, such as size,
  values, sortedness, etc.
  %
  To handle all these theories, our system interacts with a series of
  off-the-shelf constraint solvers and theorem provers, such as Z3
  \cite{Moura:TACAS:2008}, Omega Calculator \cite{Pugh:SC:1991}, MONA
  \cite{Elgaard:CAV:1998}, etc.
\end{itemize}

\begin{figure}[t!]
  \centering
  \includegraphics[scale=0.45]{figures/hip-sleek-overview.pdf}
  \caption{HIP/SLEEK overview: HIP checks whether the code verifies against
    its attached pre-/post-conditions, discharging proof obligations to
    SLEEK. In its turn, SLEEK relies on a number of off-the-shelf
    constraint solvers and theorem provers. The solid arrows show the
    workflow of the verification, and the dashed arrows are used to denote inference of predicates, lemmas and/or pre-/post-specifications).}
  \label{fig.overview}
\end{figure}

\subsection{The Programming Language}
A program \code{\mathit{\Prog}}
(\figref{fig.prelim.lang.syntax}) contains user-defined data structures
(\code{\mathit{data}}) and methods (\code{\mathit{method}}). Each method is
decorated with pre-/post-specifications describing the program's
functionality and safety conditions. A method can take both
 pass-by-reference (\code{\btt{ref}~ t~v}) parameters as well as
 pass-by-value ones (\code{t~v}). Memory is allocated using the \code{\btt{new}} operator, and \code{v.f}. is used to dereference data structures fields.
The underlying language also supports loops
(decorated with loop invariants)
but treats them as tail-recursive calls (and translates the loop
invariants into sets of pre-/post-specifications). 

\input{figures/gen_syntax}

Data structures are defined in HIP/SLEEK as follows:

\begin{minipage}[c]{\textwidth}
  \lstset{
    numbers=none,
    numberfirstline=true,
    numberblanklines=false
  }
\begin{lstlisting}{language=customc,numbers=none}
  data node { int val; node next;}
  data tree { int val; tree left; tree right;}
\end{lstlisting}

\end{minipage}

A program which recursively appends two linked-lists may be written as follows:

\begin{minipage}[c]{\textwidth}
  \lstset{
    numbers=left,
    numberfirstline=true,
    numberblanklines=false
  }
  \begin{lstlisting}{language=customc}
    void append(node x, node y)
    {
      if (x.next==NULL) x.next = y;
      else {
        // x.val = 0;
        append(x.next,y);
      }
    }
  \end{lstlisting}

\end{minipage}

Although HIP/SLEEK can
verify concurrent programs too, this paper only describes the verification of programs in the sequential setting. Details on the extension of HIP/SLEEK with a session logic to verify communication centered programs are found at \cite{costea2015,costea2017,costea2018}, while details on the verification of barriers and other synchronization mechanisms are found at \cite{khanh2013,khanh2013b,khanh2015,sharma2015}. If the reader is further interested in how to use HIP/SLEEK for program repair, \cite{Toan2019,Toan2021} details that too. 

\subsection{ The Specification Language} \label{sec:spec}

The specification language (see \figref{fig.spec_syntax}) is built on an
extension of Separation Logic \cite{reynolds2002separation,ishtiaq2001bi}
with support for separation conjunction \sep\ and inductive predicates.
%
The spatial formula \code{\heap_1 * \heap_2} asserts that the heap can be
split into two disjoint parts in which \code{\heap_1} and \code{\heap_2}
hold, respectively.
%
Moreover, \code{\node{u}{d}{\vect{v}}} is a singleton heap (or heaplet)
modelling a single data structure having \code{u} as its root address and
\code{\vect{v}} as values of its fields.
%
For example, the formula \code{\node{u}{node}{\mathrm{0},v}} represents a
data structure of type \code{node} whose first field (\code{val}) has the
value $0$, and the second field (\code{next}) is a pointer \code{v}.
%
Furthermore, \code{\view{\pred}{\vect{v}}} is an inductive heap 
modelling a recursive data structure.
%
In our work, by convention, the first argument in \code{\vect{v}} is the
"root" pointer to the specified data structure that guides the data
traversal in \code{\view{\pred}{\vect{v}}}.
%
Besides the spatial and first order logic operators, the specification
language also allows the user to write arithmetic and bag constraints.

\input{figures/fig_spec_syntax}

 The descriptions of the
data structures come in the form of
 inductive predicates which capture the shape
 and numerical properties of the underlying data structures. Since we do
 not restrict what data structures the program uses, we also do not
 restrict their corresponding predicates -- thus the user has the freedom to
 write the necessary predicates as they obey some well-foundness conditions
 (detailed later in the section). For example, the predicates below inductively
 describe
 {\bf the shape} of a linked-list and a
 linked-list segment, respectively:\\


 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{rcl}
     \code{\view{ll}{x}}
       & \code{\defeq}
       & \code{x{=}\nil \vee \exists q \cdot (\node{x}{node}{\_,q} * \view{ll}{q}).}
       \\
       \code{\view{lseg}{x,p}}
       & \code{\defeq}
       & \code{x{=}p \vee \exists q \cdot (\node{x}{node}{\_,q} * \view{lseg}{q,p}).}
   \end{array}
   $
 \end{minipage}~\\

 The \code{ll} (or \code{lseg})
 predicate asserts that a linked-list (or a linked-list
 segment) can be \emph{empty} when the root pointer to the structure is
 \code{\nil}, \code{x{=}\nil} (or equal to the pivot node \code{x{=}p}),
 or \emph{non-empty} when the root pointer refers to a \code{node} whose
 \code{next} field points to a linked-list (or linked-list segment,
 respectively). The separating conjunction in the inductive case ensures
 that the head and the tail of the linked-list structure reside in disjoint
 heaps. We use underscore (\code{\_}) in the formulae to denote an existentially
 quantified (anonymous) variable. All the non-parameter variables in
 the definition of
 a predicate are existentially quantified, even when not explicitly stated
 as so.
 The specification for the \code{append} code given in the previous
 subsection, may now be expressed as:\\

 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{ll}
     \code{\requires}
     & \code{\view{ll}{x} * \view{ll}{y} \wedge x{\ne}\nil} \\
     \code{\ensures}
     & \code{\view{ll}{x};}
   \end{array}
   $
 \end{minipage}~\\

 However, even though this specification is correct, it is actually too weak
 to capture the gist of the append operation. Instead we could write the following more
 precise specification where the pre-condition on the \code{y} parameter is a weaker
 list segment (instead of null-terminated \code{\view{ll}{y} } list):\\

 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{ll}
     \code{\requires}
     & \code{\view{lseg}{x,\nil} * \view{lseg}{y,q} \wedge
       x{\ne}\nil}\\
     \code{\ensures}
     & \code{\view{lseg}{x,q};}
   \end{array}
   $
 \end{minipage}~\\

 These two definitions (\code{ll} and \code{lseg}) can be refined into capturing more precise
 information about the underlying structures, such as {\bf the size} of the lists:
\\

 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{rcl}
     \code{\view{ll_n}{x,n}}
     & \code{\defeq}
     & \code{(x{=}\nil \wedge n{=}\mathrm{0})  \vee \exists q \cdot (\node{x}{node}{\_,q} * \view{ll_n}{q,n{-}1})}
     \\
     && \code{inv ~n{\ge}\mathrm{0}.}\\
     \code{\view{lseg_n}{x,p,n}}
     & \code{\defeq}
     & \code{(x{=}p \wedge n{=}\mathrm{0}) \vee \exists q \cdot
       (\node{x}{node}{\_,q} * \view{lseg_n}{q,p,n{-}1})}\\
     && \code{inv ~n{\ge}\mathrm{0}.}\\
   \end{array}
   $
 \end{minipage}~\\


 The above definitions also specify a default invariant \code{n{\ge}\mathrm{0}}
 which holds for all \code{ll_n} and \code{lseg_n} list predicates.
 This predicate invariant can be verified by checking that each disjunctive
 branch of the predicate
 definition always implies its stated invariant.
 Types need not be given in our specification as we have an
 inference algorithm to automatically infer non-empty types for
 specifications that are well-typed. For the above predicates,
 our type inference can
 determine that \code{n} is of \code{int} type,
 while \code{x}, \code{q} and \code{p} are of \code{node} type.
 The specification for the \code{append} method could now capture even more
 information with this improved definitions:\\

 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{ll}
     \code{\requires}
     & \code{ \view{ll_n}{x,n} * \view{ll_n}{y,m} \wedge
       x{\ne}\nil} \\
     \code{\ensures}
     & \code{ \view{ll_n}{x,n{+}m};}\\
     \code{\requires} &
     \code{\view{lseg_n}{x,\nil,n} * \view{lseg_n}{y,q,m} \wedge
       x{\ne}\nil~}
     \\
     \code{\ensures} & \code{\view{lseg_n}{x,q,n{+}m};}\\
   \end{array}
   $
 \end{minipage}~\\

 The \code{append} method verifies against these specifications.
 But let us consider a  scenario for the append method
 given earlier
 where the instruction at line  \code{5} is uncommented. 
 That is,
 the bogus statement \code{x.val = 0} is now assumed to be
 a part of the append’s method body.
 So the considered method now also resets the
 values stored in the list segment pointed by \code{x}. To avoid this bogus
 behavior we could refine further the definition of a list segment and the
 pre-/post-specifications as follows:\\

 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{rcl}
     \code{\view{lseg_B}{x,p,n,B}}
     & \code{\defeq}
     & \code{(x{=}p \wedge n{=}\mathrm{0} \wedge B{=}\emptyset)~ \vee}\\
     \multicolumn{3}{l}{\code{\qquad \qquad \qquad \qquad
       \exists q,v \cdot
       (\node{x}{node}{v,q} * \view{lseg_B}{q,p,n{-}1,B_1} \wedge
       B{=}B_1\cup\{v\})}}\\
     \multicolumn{3}{l}{\code{\qquad \qquad \qquad \qquad
       \text{inv} ~n{\ge}\mathrm{0}.}}\\
   \end{array}
   $
 \end{minipage}
 \\~\\~\\
 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{ll}
     \code{\requires} &
     \code{\view{lseg_B}{x,\nil,n,B_x} * \view{lseg_B}{y,q,m,B_y} \wedge
       x{\ne}\nil~}\\
     \code{\ensures} & \code{\view{lseg_B}{x,q,n{+}m, B_x\cup B_y};}\\
   \end{array}
   $
 \end{minipage}~\\

\noindent which besides the shape and size properties, it also captures
{\bf the
values} stored in the linked-lists. Going even further with these refinements,
one could also define the {\bf sortedness} property of the linked-list segment:\\

\begin{minipage}[c]{\textwidth}
  $
  \begin{array}{rcl}
    \code{\view{lsort}{x,p,n,B}}
    & \code{\defeq}
    & \code{(x{=}p \wedge n{=}\mathrm{0} \wedge B{=}\emptyset)~ \vee}\\
    \multicolumn{3}{l}{\code{\qquad \qquad \qquad \qquad
        \exists q,v \cdot
        (\node{x}{node}{v,q} * \view{lsort}{q,p,n{-}1,B_1} \wedge
        B{=}B_1\cup\{v\})}}\\
    \multicolumn{3}{l}{\code{\qquad \qquad \qquad \qquad
        \wedge~ \forall w \cdot (w \notin B_1 \vee v {\le} w) }}\\
    \multicolumn{3}{l}{\code{\qquad \qquad \qquad \qquad
        \text{inv} ~n{\ge}\mathrm{0}.}}\\
  \end{array}
  $
\end{minipage}~\\

\noindent The program's specification in this case can be written as:\\

\begin{minipage}[c]{\textwidth}
  $
  \begin{array}{ll}
    \code{\requires} &
    \code{\view{lsort}{x,\nil,n,B_x} * \view{lsort}{y,q,m,B_y} \wedge
      x{\ne}\nil~ \wedge}\\
    \multicolumn{2}{r}{\code{\qquad\qquad\qquad\qquad \forall v_x,v_y \cdot ((v_x\in B_x \wedge v_y \in
      B_y) \vee (v_x {\le} v_y))}}\\
    \code{\ensures} &\code{ \view{lsort}{x,q,n{+}m, B_x\cup B_y};}\\
  \end{array}
  $
\end{minipage}~\\

\noindent which ensures that if the two lists given as input are sorted,
and, moreover, all the elements in the list pointed by \code{x} are
less  than or equal to the elements in the list segment pointed by
\code{y},
then the resulting list segment is still sorted. ~\\

\noindent{\emph{\bf Multiple Specifications.}}
Each method may be decorated with
multiple pairs of pre-/post-conditions. If that is the case, a method
definition needs to verify against every pair of associated specifications,
while a callee needs to entail at least one pre-condition associated with
the corresponding method call (or at least one pre-conditions corresponding to 
each context of the method call if the same call statement needs to be verified from different contexts). In other words, the caller may choose the
pair of specification(s) which caters best for its calling environment.
For the \code{append} method, all of the specifications provided so far
verify against the method's body, hence a user may very well attach all of
the earlier specifications to \code{append}. The technical details behind
this enhancement are found in \cite{chin2007multiple}.~\\

\noindent{\emph{\bf Immutability Specifications.}} It is often the case
that a method only mutates some parts of its memory footprint.
 To capture this fact in the specification, HIP supports a logic of
 immutability annotations, where data structures are annotated as
 \emph{mutable} -- \code{@M}, or \emph{immutable} -- \code{@L} according to
 the case. We define a subsumption relation between these annotations,
 where \code{@M {<:} @L}, meaning that a heap which is annotated as mutable
 may be both read or mutated, while an immutable heap may only be used for
 reading operations. There are a number of benefits from using 
 immutability annotations, such as concise specifications, or increased expressivity as compared to the specifications with no immutability annotations. There is an
 exhaustive list of these benefits and the technical details behind the
 immutability annotations in \cite{david2011immutable}. Furthermore, it is
 also the case that a method may only operate on certain
 fields of a given data structure. This fact may be described using
 immutability annotations at the field level rather than at the data structure
 level, e.g. a list should maintain its shape intact while being
 passed to a method that only resets its values.
 The details of how HIP achieves this are found in \cite{costea2014hipimm}.

 Coming back to the \code{append} method, one could now write a more compact
 specification which captures the immutability of both the shape and the values
 of the list segment pointed by \code{y} as follows:~\\


 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{ll}
     \code{\requires} &
     \code{ \view{lseg}{x,\nil} * \view{lseg}{y,q}@L \wedge
       x{\ne}\nil\qquad}\\
     \code{\ensures} & \code{\view{lseg}{x,q};}\\
   \end{array}
   $
 \end{minipage}~\\

\noindent
 where the properties of the list segment pointed by \code{y} need not be
 proved, hence missing from the post-condition, but just assumed to 
 hold since this data structure was marked as immutable.~\\

 \noindent{\emph{\bf Structured Specifications.}} We show in
 \cite{gherghina2011structured} how adding structure to a flat
 specification leads to better expressiveness  and verifiability.
 In particular, structured specifications allow a verifier to perform a
 case analysis in order to take advantage of the disjointness conditions in
 the logic.
 % To briefly emphasize that, let us consider a data structure
 % to construct an AVL tree and a code snippet which returns the
 % height of the tree.
 ~\\

 \noindent{\emph{\bf Specifications with the Explicit Notion of Infinity.}}
 We show in \cite{sharma2015certified} the benefits of adding the notion
 of infinity to the specification language. In particular, the
 specifications are not only more expressive, but also more human readable
 and more concise, leading to better composability.
 For example, the sortedness property of linked-list segments may also be captured
 via the minimum value property when one chooses to avoid the expensive
 reasoning over bags of values:~\\

 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{rcl}
     \code{\view{lsort_{min}}{x,p,mn}}
     & \code{\defeq}
     & \code{(\node{x}{node}{mn,p})~ \vee}\\
     \multicolumn{3}{l}{\code{\qquad \qquad \qquad
         \exists q,mn_1 \cdot
         (\node{x}{node}{mn,q} * \view{lsort_{min}}{q,p,mn_1} \wedge
         mn{\le}mn_1).}}\\
   \end{array}
   $
 \end{minipage}~\\

 \noindent but this definition forces the list segment to be non-empty. To
 support non-empty case too and to avoid disjunctive specifications which
 should have dedicated assertions for the empty case, one would write the
 following definition using the special ghost variable \code{\infty}:~\\

 \begin{minipage}[c]{\textwidth}
   $
   \begin{array}{rcl}
     \code{\view{lsort^{\infty}_{min}}{x,p,mn}}
     & \code{\defeq}
     & \code{{x}{=}{p}~ \wedge mn{=}\infty \vee}\\
     \multicolumn{3}{l}{\code{\qquad \qquad \qquad
         \exists q,mn_1 \cdot
         (\node{x}{node}{mn,q} * \view{lsort^{\infty}_{min}}{q,p,mn_1} \wedge
         mn{\le}mn_1).}}\\
   \end{array}
   $
 \end{minipage}~\\

 \noindent{\emph {\bf Well-formedness.}} As highlighted before,
 separation formulae are used in pre/post-conditions and shape definitions.
 In order to handle them correctly without running into
 unmatched residual heap nodes,
 we require each separation constraint to be well-formed, as defined below:
 \begin{definition}[Accessible]
   A variable is accessible if it is a method parameter, or if it is a
   dedicated variable, such as \code{res}.
 \end{definition}

 \begin{definition}[Reachable]
   Given a heap constraint \code{\heap} and a pointer constraint
   \code{\ptr},
   the heap nodes
   in \code{\heap} that are reachable from a set of
   pointers \code{S} can be computed by the following recursively defined
   function:\\
   \[
   \begin{array}{lcl}
     \code{reach(\heap,\ptr,S)} & \code{\defeq} &
     \code{\node{u}{d}{\vect{v}}* reach(\heap-(\node{u}{d}{\vect{v}}),\ptr,
       S\cup\{v|v{\in}{\vect{v}}, IsPtr(v)\})}\\
     \multicolumn{3}{r}{ \qquad \text{when}~ \code{\exists q{\in}S \cdot (\ptr \implies u{=}q)
       \wedge \node{u}{d}{\vect{v}} \in \heap }}\\
     \code{reach(\heap,\ptr,S)} & \code{\defeq} &
     \code{\view{\pred}{\vect{v}}* reach(\heap-(\view{\pred}{\vect{v}}),\ptr,
       S\cup\{v|v{\in}{\vect{v}}, IsPtr(v)\})}\\
     \multicolumn{3}{r}{ \qquad \text{when}~ \code{\exists q{\in}S, p{\in}\vect{v} \cdot (\ptr \implies p{=}q)
         \wedge \view{\pred}{\vect{v}} \in \heap }}\\
   \code{reach(\heap,\ptr,S)} & \code{\defeq} &
   \code{\emp} \quad \text{otherwise.}
   \end{array}
   \]

 \noindent Note that \code{\heap-\heap'} removes a term \code{\heap'} from
 \code{\heap}, and \code{IsPtr(v)} determines if \code{v} is of type
 pointer.
\end{definition}

\begin{definition}[Well-formed formula]
  A separation formula is well-formed if:
  \begin{itemize}
  \item it is in a disjunctive normal form
    \code{\bigvee_i(\exists \vect{v} \cdot \heap_i  \wedge \pure_i )};
    \item all occurrences of heap nodes are reachable from its accessible
      variables, S. That is, we
      have \code{\forall i \cdot \heap_i = reach(\heap_i,\ptr_i,S)} modulo associativity and commutativity of the separation
      conjunction \code{*}.
  \end{itemize}
\end{definition}

The
primary significance of the well-formed condition is that all heap nodes of a heap constraint are
reachable from accessible variables. This allows the entailment checking procedure to correctly
match nodes from the consequent with nodes from the antecedent of an
entailment relation.

\begin{definition}[Well-founded Predicate]
  A shape predicate is said to be \emph{well-founded}
  if it satisfies the following conditions:
  \begin{itemize}
    \item its body is a well-formed formula;
    \item given \code{u} as the special parameter which denotes the
      predicate's root pointer, if the body of the predicate contains some
      spatial formula, then \code{u} must be a heaplet of the form
      \code{\node{u}{d}{\vect{v}}}.
  \end{itemize}
\end{definition}

Informally, the primary significance of the well-founded predicates is to
ensure the monotonicity of the predicates which in turn guarantees
the existence of a descending chain of unfoldings.
The need for this well-founded conditions is clearer
in \secref{sec:entailment} while showing how SLEEK solves the proof
obligations generated by HIP.

\subsection{ Forward Verification Rules} \label{sec:verif_rules}


The proof system is formalized using Hoare triples of the following
form: \code{\htriple{\St_{pre}}{e}{\St_{post}}}, where
\code{e} is the expression to be verified, the pre-state \code{\St_{pre}} is given and
the post-state \code{\St_{post}} is computed.
We next generalize this triple to support a set of possible
post-states:  % instead of just one post state:
\code{\htriple{\St_{pre}}{e}{\resid}},
where \code{\resid}
is a residual set of heap states
discovered by the proof-search-based strategy
adopted during the verification process.
The verification is said to have succeeded
with \code{\St_{pre}} as prestate
if the residual set \code{\resid} is non-empty, and failed
otherwise.

The verification rules are given in \figref{fig.prelim_verification}.
The proof system engages a special variable \code{res}
to denote the result of evaluating the target expression.
\code{\rho}
represents substitution, and \code{\Prog} is the program
being verified. The rules
\code{\rulen{FIELD-READ}},
\code{\rulen{FIELD-UPDATE}},
\code{\rulen{METH-DEF}},
\code{\rulen{METH-CALL}}
discharge proof obligations in the form of entailment relations written as
\code{\subtr{\St_1}{\St_2}{\resid}} and read as "if \code{\St_1} is true,
then \code{\St_2} is also true with the possible residual states \code{\resid}".
In other words, the residual states are the witnesses that the respective proof
obligation holds. The technicalities behind the entailment checks are
postponed to \secref{sec:entailment}.

\input{figures/fig_verification}

Most of the verification rules are standard. 
%
\code{\rulen{METH-DEF}} shows how to verify a method definition annotated with \code{p} pairs of specifications.
For each pair \code{i} of specifications, the verifier starts by assuming the method’s \code{i}-th precondition, namely \code{\pre^i}. It then incrementally applies the other verification rules to verify that symbolically executing 
the method's body \code{e} leads to a set of post-states \code{\resid_1^i}  each of which entails the corresponding post-condition \code{\post^i}.
To note that a method takes in \code{m} pass-by-reference parameters and 
\code{n-m} pass-by-value parameters. To capture how the method's body changes the values of the pass-by-reference parameters, we use the primed notation, e.g. for a parameter \code{v}, we use \code{v'} to denote 
its latest (current) value. 
The $\mathit{nochange}$ function initializes the current values of parameters to their initial (unprimed) values (the precondition \code{\pre^i} is given only in terms of unprimed variables).  
At the end of the procedure, the current (primed) values of the pass-by-value parameters are existentially quantified from the post-state \code{\post^i} so that their values are not visible by the postcondition, hence by the callers of the procedure. 
%

\code{\rulen{METH-CALL}} is the rule for method call.  At a high level, if a pre-condition, \code{\pre^i}, is satisfied at the call site
then its corresponding post-condition, \code{\post^i}, is added to the state. The pass-by-value parameters, \code{V}, are equated to their initial values through the
$\mathit{nochange}$  function, since their final values are not visible to the method’s callers. The
residual heap state for each pair of specification, \code{\resid^i} , from checking the method’s \code{i}-th pre-condition is composed with its corresponding post-condition to become the post-state, \code{\resid} , of the method call.


For brevity, we lifted the binary operations normally used for formulae
composition, namely \code{*,~\wedge,~\vee}, to composition of sets and
formulae in order to precisely capture the residue of each proof rule (note
that the separation conjunction operator \code{*} is commutative,
associative, and distributive over disjunction). The normalization patterns that HIP/SLEEK often engages are presented in
\figref{fig.prelim_normalization}.
%


\begin{figure}[t]
\renewcommand{\arraystretch}{1.2}
\begin{tabular}{lllllll}
  $\pure \wedge \resid$
  & $\equiv$
  & $\{\St_s \wedge \pure ~|~ \St_s \in \resid\}$
  & \hspace*{1em}
  & $(\heap_1 \wedge \pure_1) * (\heap_2 \wedge \pure_2)$
  & $\equiv$
  & $\heap_1 * \heap_2 \wedge \pure_1 \wedge \pure_2$ \\

  $\resid \wedge \pure$
  & $\equiv$
  & $\{\St_s \wedge \pure ~|~ \St_s \in \resid\}$
  &
  & $\resid_1 \vee \resid_2$
  & $\equiv$
  & $\{\St_1 \vee \St_2 ~|~ \St_1 \in \resid_1,\,\St_2 \in \resid_2\}$ \\

  $\St * \resid$
  & $\equiv$
  & $\{\St_s * \St ~|~ \St_s \in \resid\}$
  &
  & $\exists v \cdot (\resid \wedge \pure)$
  & $\equiv$
  & $\{\exists v \cdot (\St_s \wedge \pure) ~|~ \St_s \in \resid\}$ \\

  $\resid * \St$
  & $\equiv$
  & $\{\St_s * \St ~|~ \St_s \in \resid\}$
  &
  & $(\exists x \cdot \St) \wedge \pure$
  & $\equiv$
  & $\exists y \cdot ([y/x]\St) \wedge \pure$ \\


  $(\St_1 \vee \St_2) \wedge \pure$
  & $\equiv$
  & $(\St_1 \wedge \pure) \vee (\St_2 \wedge \pure)$
  &
  & $(\exists x \cdot \St_1) * \St_2$
  & $\equiv$
  & $\exists y \cdot ([y/x]\St_1) * \St_2$ \\

\end{tabular}
\caption{Normalization Rules}
\label{fig.prelim_normalization}
\end{figure}




%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
