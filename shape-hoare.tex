To support specification inference via second-order bi-abduction, we extend
the Hoare-style forward rule presented in Section \ref{sec:verification} to the form:
\hoareI{v^*}{\pstate{1}}{c}{\prestate{2}}{\pstate{2}}{\reldef{2}}
with three 
additional features (i) a set of specified variables 
\form{[v^*]} 
(ii) an extra precondition \form{\prestate{2}} that must be added
(iii) a set of definitions 
and obligations \form{\reldef{2}} on the uninterpreted
relations. The selectivity criterion will help ensure
that \form{\prestate{2}} and \form{\reldef{2}} come
from only the specified set of variables, namely \form{\{v^*\}}.
If this set is empty,
our new rule is simply a special case that 
only performs verification, without any inference.


%% Rule \form{\infrule{SEQ}} shows how sequential composition
%% \form{e_1; e_2} is being handled. The two inferred preconditions
%% are conjunctively combined as
%% \form{\prestate{2} \wedge \prestate{3}}.
%% Rule \form{\infrule{IF}} shows how conditional expression
%% is being handled. Our core language allows
%% only variables (e.g. \form{w}) in each
%% conditional test. We use a primed notation 
%% whereby \form{w} denotes the old value,
%% and \form{w'} denotes the latest value of each
%% variable \form{w}.
%% The conditions \form{w'} and \form{{\neg}w'}
%% are asserted for each of the two conditional
%% branches, respectively. As only one of the two branches will
%% be executed, the inferred preconditions 
%% \form{\prestate{2},\prestate{3}}
%% are combined conjunctively 
%% %\tmsay{Check this} 
%% in a conservative manner.

%% Rule \form{\infrule{ASSIGN}} deals with assignment statement. 
%% We first define a {\em composition with update} operator. 
%% Given a state \pstate{1}, a state change \pstate{2}, and a set of variables to be
%% updated $X{=}~\{x_1,\dots,x_n\}$, the composition operator $\code{op}_X$ is defined as:
%% $\pstate{1}~{\code{op}_X}~\pstate{2}~{\defs}~ \exists~ r_1..r_n \cdot (\rho_1 \pstate{1})~\code{op}~(\rho_2 \pstate{2})$,
%% where $r_1,\dots,r_n$ are fresh variables and $\rho_1~{=}~[r_i/x_i']_{i=1}^n$, $\rho_2~{=}~[r_i/x_i]_{i=1}^n$.
%% Note that $\rho_1$ and $\rho_2$ are substitutions that link each latest value of $x_i'$ in \pstate{1} with the corresponding
%% initial value $x_i$ in \pstate{2} via a fresh variable $r_i$. 
%% The binary operator \code{op} is either $\wedge$ or $\sep$.
%% Instances of this operator will be used in the inference rule 
%% for assignment (as $\wedge_u$ in \form{\infrule{ASSIGN}}) 
%% and in the inference rule for method invocation (as ${\sep}_{V{\cup}W}$ in \form{\infrule{METH{-}CALL}}).

\input{shape-rule-hoare}

Figure~\ref{fig.hoare} captures a subset of
 our Hoare rules with
 bi-abduction. 
For each method call, we must ensure that its
precondition is satisfied, and then add the
expected postcondition into its residual state, as illustrated in
\form{\infrule{METH{-}CALL}}.
Here, $(t_i~v_i)_{i=1}^{m-1}$ are pass-by-reference parameters, which are marked with \code{ref},
while the pass-by-value parameters $V$ are equated to their initial values through the
\nochange~function, as their updated values are not visible 
in the method’s callers.
Note that
inference may occur
during the entailment of 
the method's precondition. 

Lastly, we discuss 
the rule for 
handling each method declaration \form{\infrule{METH{-}DEF}}.
At the program level, our inference rules will be applied
to each set of mutually-recursive methods in a bottom-up
order in accordance with the call hierarchy. This allows us
to gather the entire set \sm{\reldef{}} of relational assumptions
for each uninterpreted relation.
For a shape-based relation, we make use of the algorithm presented
in \cite{Loc:CAV:2014} to derive a definition for it.
For a pure relation, we infer the pre- and post-relations via the two steps described below.
Take note that, given the entire set \sm{\reldef{}}, 
we retrieve the set of definitions and obligations for post-relations through 
functions \code{def_{po}} and \code{obl_{po}} respectively,
while we use functions \code{def_{pr}} and \code{obl_{pr}} for pre-relations.

\[
\begin{array}[t]{lllll}
\code{def_{po}}(\reldef{}) & {=} & \{\pure_i^k {\to} v_{rel_i}(v^*_i) & {|} & (\pure_i^k {\to} v_{rel_i}\sm{\code{@po}}(v^*_i))~{\in}~\reldef{}\}\\
\code{obl_{po}}(\reldef{}) & {=} & \{v_{rel_i}(v^*_i){\to}\atomic{j}     & {|} & (v_{rel_i}\sm{\code{@po}}(v^*_i){\to}\atomic{j})~{\in}~\reldef{}\}\\
\code{def_{pr}}(\reldef{}) & {=} & \{\pure_i^k {\to} v_{rel_i}(v^*_i) & {|} & (\pure_i^k {\to} v_{rel_i}\sm{\code{@pr}}(v^*_i))~{\in}~\reldef{}\}\\
\code{obl_{pr}}(\reldef{}) & {=} & \{v_{rel_i}(v^*_i){\to}\atomic{j}     & {|} & (v_{rel_i}\sm{\code{@pr}}(v^*_i){\to}\atomic{j})~{\in}~\reldef{}\}\\
\end{array}
\]

\begin{itemize}
\item In order to infer post-relations, the function \myit{infer\_post} 
applies a least fixed point analysis 
over the sets %% of gathered relational definitions
\sm{\code{def_{po}}(\reldef{})} and \sm{\code{obl_{po}}(\reldef{})}.
For computing the least fixed point in the two domains used in
the current inference framework, 
namely the numerical domain and the set/bag domain, 
we utilize \fixcalc \cite{Popeea:ASIAN06} and
\fixbag \cite{Pham:CAV2011}, respectively.
%% The call to the fixed point analysis is denoted below as \sm{\btt{LFP}(\code{def_{po}}(\reldef{}))}, 
%% It takes as inputs the set of relational definitions, while returning a set of 
%% closed form constraints of the form \sm{\atomic{i} {\to} v_{rel_i}(v^*_i)},
%% where each constraint corresponds to an uninterpreted relation \sm{v_{rel_i}}.
%% Given that we aim at inferring strongest post-relations,
%% we further consider each post-relation \sm{v_{rel_i}(v^*_i)} to be equal to \sm{\sm{\atomic{i}}}.
%% Finally, \myit{infer\_post} returns a set of substitutions, where each uninterpreted relation is to be substituted by the inferred formula, given that this formula satisfies all the 
%% relational obligations from \sm{\code{obl_{po}}(\reldef{})} corresponding to that particular relation. 
%% \[
%% \begin{array}[t]{c}
%% \myit{infer\_post}(\reldef{}){=}\{ \atomic{i}/v_{rel_i}(v^*_i)~|~  
%% (\atomic{i} {\to} v_{rel_i}(v^*_i)){\in}\btt{LFP}(\code{def_{po}}(\reldef{}))\\
%% \phantom{XXXXXXXXX}{\wedge}~ \forall (v_{rel_i}(v^*_i) {\to} \atomic{j}) {\in} \code{obl_{po}}(\reldef{}) 
%% {\cdot} \atomic{i} {\Rightarrow} \atomic{j}\}  
%% \end{array}
%% \]
\item For pre-relations, our goal is to infer the weakest preconditions via
\myit{infer\_pre}. 
Hence, for each pre-relation, we first calculate the conjunction of
all its obligations from \sm{\code{obl_{pr}}(\reldef{})} 
to obtain sufficient preconditions %% \code{pre\_fst_i} 
for base cases.
To capture the precondition for a recursive call,
we need to derive the {\em recursive invariant}
which can be achieved via a top-down fixed point analysis \cite{Popeea:PEPM08}.
%% Obviously, the parameters of an arbitrary call 
%% must also satisfy relevant relational obligations (e.g. \code{pre\_fst_i}).
%% Finally, the approximation $\atomic{i}$ for each relation $v_{rel_i}(v^*_i)$
%% must satisfy the precondition for the first call (\code{pre\_fst_i}),
%% for an arbitrary recursive call (\code{pre\_rec_i}).
%% The last step is to check the quality of candidate preconditions
%% in order to keep the ones that satisfy not only
%% the obligations but also definitions of each relation.

%% \[
%% \begin{array}[t]{c}
%% \code{pre\_fst_i} = \{ \wedge_j \atomic{j}~|~(v_{rel_i}(v^*_i) {\to} \atomic{j}) {\in} \code{obl_{pr}}(\reldef{})\}\\
%% \code{rec\_inv} = \btt{TDFP}(\code{def_{pr}}(\reldef{}))\\
%% \code{pre\_rec_i} = \forall (\FV(\code{rec\_inv_i}){-}v_i^*) \cdot (\neg\code{rec\_inv_i}~{\lor}~\code{pre\_fst_i})\\
%% \atomic{i} = \code{pre\_fst_i}~{\land}~\code{pre\_rec_i}~{\wedge}~\code{INV}\\
%% \res~=~\code{sanity\_checking}(\{\atomic{i}/v_{rel_i}(v^*_i)\},\code{obl_{pr}},\code{def_{pr}})\\
%% \hline
%% \myit{infer\_pre}(\reldef{})~=~\res
%% \end{array}
%% \]

~\\

Through the functions \myit{infer\_pre} and \myit{infer\_post}, 
we can finally infer definitions of
uninterpreted relations for deriving the pre- and postconditions, 
\form{{\pre^n}} and \form{{\post^n}}, of a method \myit{mn}.
Note that \form{v_{rel}^*} denotes the set of uninterpreted relations that are
to be inferred, whereas $\rho_1$ and $\rho_2$ represent the 
substitutions obtained for pre- and post-relations, respectively.

\end{itemize}

