% \subsection{The entailment proof system}

The proof obligations generated in Sec \ref{sec:verification}, abbreviated as
{\it heap entailments}, are handled by our entailment prover SLEEK. The
formulas of the entailments are a combination of separation logic and
heap-independent logics, and are of the form

\begin{center}
  $\St_A \vdash^{\kappa}_V \St_C * \St_R$
\end{center}


\noindent which is a consequence (or shortcut) for

\begin{center}
  $\kappa * \St_A \vdash \exists V \cdot (\kappa * \St_C) * \St_R$
\end{center}

To prove the above entailment, we need to check whether the heaplet modelled by
the antecedent $\St_A$ is sufficiently precise to cover all heaplets
modelled by the consequent $\St_C$. Furthermore, we also aim to compute the
{\it residual heap state} $\St_R$ (a.k.a the ``frame'' condition
\cite{Calcagno:POPL:09}), which represents what was not consumed from the
antecedent after matching up with the formula form the consequent. $\kappa$
is the history of nodes from the antecedent that have been used to match
nodes from the consequent, $V$ is the list of existentially
quantified variables from the consequent. Note that $\kappa$ and $V$
are derived during the entailment proof. The entailment checking procedure
is initially invoked with $\kappa = \emp$ and $V = \emptyset$. The
entailment proving rules are explained as follows.

\paragraph{\textbf{Handling entailments of disjunctive formulas}}

Firstly, we present the reduction from entailment between disjunctive
formulas with existential quantifiers to entailment between quantifier-free
conjunctive formulas.

\emph{Removing disjunction.} An entailment with a disjunctive antecedent
succeeds if both disjuncts entail the consequent (\rulename{ENT-LHS-OR}).
Conversely, an entailment with disjunctive consequent succeeds if either one of the
disjuncts succeeds (\rulename{ENT-RHS-OR}).

\begin{center}
  \small
  \begin{tabular}{c}
    \minipageEntailDisjunctLeft
    \hspace{3em}
    \minipageEntailDisjunctRight
  \end{tabular}
\end{center}

\emph{Removing existential quantifiers.} Existentially quantified variables
from the antecedent are simply lifted out of the entailment relation by
replacing them with fresh variables (\rulename{ENT-LHS-EX}). On the other
hand, we keep track of the existential variables coming from the consequent
by adding them to $V$ (\rulename{ENT-RHS-EX}).

\begin{center}
  \small
  \begin{tabular}{c}
    \minipageEntailExistsLeft
    \hspace{2em}
    \minipageEntailExistsRight
  \end{tabular}
\end{center}

\paragraph{\textbf{Handling entailments of conjunctive formulas}}

We now present the reduction of entailment between two quantifier-free
conjunctive formulae to entailment between two pure formulae.

\emph{Handling consequent with empty heap.} The base case for our
entailment checker occurs when the consequent is a pure formula, in which
case the \rulename{ENT-EMP} rule is applied. The rule first approximates the
antecedent of the entailment, including the heap formulae that have been
matched previously and kept in $\kappa$. The approximation is done by the
$\text{XPure}$ function which taken a heap formula as input and outputs its pure approximation,
e.g. the information that a pointer $p$ points to a valid memory location, $\node{p}{node}{x,y}$, 
is approximated by the pure formula $\code{p} \neq \nil$. The full definition of $\text{XPure}$ is deferred to the end of this sub-chapter.
The entailment checker next invokes an off-the-shelf
theorem prover to check if the approximation of the antecedent implies the
heap-independent consequent.

\begin{center}
  \small
  \begin{tabular}{c}
    \minipageEntailEmp
  \end{tabular}
\end{center}



\emph{Matching and removing heap nodes from the antecedent and the
  consequent.} The rule \rulename{ENT-MATCH} works by successively matching
up heap nodes that can be proven aliased.

\begin{center}
  \small
  \begin{tabular}{c}
    \minipageEntailMatch
  \end{tabular}
\end{center}

In the above rule, the condition $\XPure_n(\node{u_1}{d}{\vect{v}_1} *
\kappa_1\wedge \pi_1) \Rightarrow \code{u}_1{=}\code{u}_2$ checks whether
$\code{u_1}$ and $\code{u_2}$ %% can be proved to be
are aliasing based on
the information in the antecedent. % of an entailment.
 If two atomic heap
formulas have the same name, that is, they are either two heaplets of the same
type, or two instances of the same predicate, we require their fields or, respectively, arguments to
be the same. The unification of the two heap formula is
accomplished by the application of substitution $\rho$ to the remaining of
the consequent. Since $\code{\vect{v}_2}$ is substituted away, it is
removed from the list of existentially quantified variables.

When a match occurs and an argument of the heap node coming from the
consequent is free, the entailment procedure binds the argument to the
corresponding variable from the antecedent and moves the equality to the
antecedent. In our system, free variables in consequent are variables from
method preconditions. These bindings play the role of parameter
instantiations during forward reasoning and can be accumulated into the
antecedent to allow the subsequent program state (from the residual heap
state) to be aware of their instantiated values. This process is formalized
by function $\freeEqn$, where $V$ is the set of existentially
quantified variables:

\begin{center}
  \small
  \renewcommand{\arraystretch}{1.2}
  \begin{tabular}{lll}
    $\freeEqn([\code{u}_i/\code{v}_i]^n_{i=1}, V)$
    & $\defeq$
    & $\code{let}~\pi_i = (\code{if}~ \code{u}_i \in V
    ~\code{then}~ \true ~\code{else}~ \code{v}_i = \code{u}_i)$ \\

    &
    & $\code{in} \bigvee^n_{i=1} \pi_i$
  \end{tabular}
\end{center}

For soundness, we perform a preprocessing step to ensure that variables
appearing as arguments of heap nodes and predicates are (i) distinct and
(ii) if they are free, they do not appear in the antecedent by adding
(existentially quantified) fresh variables and equalities. This guarantees
that the formula generated by $\freeEqn$ does not introduce any additional
constraints over existing variables in the antecedent.

\emph{Unfolding a shape predicate in the antecedent. } If a predicate
instance in the antecedent is aliased with an object in the consequent, we
unfold it by using the rule \rulename{ENT-UNFOLD} below.

\begin{center}
  \small
  \begin{tabular}{c}
    \minipageEntailUnfold
  \end{tabular}
\end{center}

\noindent where the function \code{unfold} is defined as follows:
 
\begin{center}
  \small
  \begin{tabular}{c}
    \minipageUnfolding
  \end{tabular}
\end{center}

The above rule basically replaces the predicate instance by its predicate
definition, normalizes the resulting formula, and resumes entailment
checking. Each unfolding either exposes an object that matches the object
in the consequent, or reduces the atomic heap formula in the antecedent
$\node{u_1}{d}{\vect{v}_1}$ to a pure formula. The former case results in a
reduction of the consequent by using \rulename{ENT-MATCH}. In the latter
case, the entailment either (i) fails immediately since the checker is
unable to find an aliased heap node, or (ii) if the obtained pure formula
reveals additional aliasing information, the entailment checker continues
with a new aliased heap node from the antecedent. If the new aliased heap
node is an object, a match and thus a reduction of the consequent occurs.
Otherwise, a new unfolding is triggered. This process cannot go forever as
every time it happens, one predicate from the antecedent is removed and no
new predicate instance is generated.

% For illustration, consider the following example, where $\St_R$ captures
% the residual of entailment.

% \begin{center}
%   \begin{tabular}{c}
%     $\view{ll_n}{x, n} \wedge \code{n}{>}3 \vdash (\exists \code{r} \cdot
%     \node{node}{x}{\_, r} *
%     \node{node}{r}{\_, y} \wedge \code{y} {\not\eq}\nil) * \St_R$
%   \end{tabular}
% \end{center}

% To prove this entailment, we would unfold the $\view{ll_n}{n}$ predicate in
% the antecedent twice to allow the two data nodes on the consequent to be
% matched up. Consequently, the residue is computed as follows.

% \begin{center}
%   \begin{tabular}{c}
%     $\exists q_1 \cdot \node{node}{x}{\_, q_1} {*} \view{ll_n}{q_1, n{-}1}
%     {\wedge} n{>}3 \vdash
%     (\exists r \cdot \node{node}{x}{\_, r} {*}
%     \node{node}{r}{\_, y} {\wedge} y{\not\eq}\nil) {*} \St_R$
%   \end{tabular}
% \end{center}


% \begin{tabular}{lll}
%   \code{\view{ll_n}{q_1, n{-}1} \wedge n{>}3} & $\vdash$
%   & \code{(\node{node}{q_1}{\_,y} \wedge y{\not\eq}\nil) * \St_R} \\
%   \code{\exists q_2  \cdot \node{node}{q_1}{\_, q_2} * \view{ll_n}{q_2, n{-}2} \wedge n{>}3}
%   & $\vdash$ &
%   \code{\node{node}{q_1}{\_, y} \wedge n{\not\eq}\nil * \St_R} \\
%   \code{\view{ll_n}{q_2, n{-}2} \wedge n{>}3 \wedge q_2{=}y} & $\vdash$
%   & \code{y{\not\eq}\nil * \St_R}
% \end{tabular}

% Note that due to the well-founded condition, each unfolding exposes a data node
% that matches the data node in the consequent. Thus a reduction of the consequent
% immediately follows, which contributes to the termination of the entailment check.

\emph{Folding against a shape predicate in the consequent.} If a predicate
instance in the consequent does not have a matching predicate instance in
the antecedent, we attempt to generate one by folding the antecedent
({\rulename{ENT-FOLD}}).

\begin{center}
  \small
  \begin{tabular}{c}
    \minipageEntailFold
  \end{tabular}
\end{center}

\noindent where the function \code{fold} is defined as follows:

\begin{center}
  \small
  \begin{tabular}{c}
    \minipageFolding
  \end{tabular}
\end{center}

Some heap nodes from $\kappa$ are removed by the entailment procedure in the $\textit{fold}$ definition
so as
to match with the heap formula of the predicate $\view{\pred}{\vect{v}}$.
This requires a special version of entailment that returns three extra
things: (i) the consumed heap nodes, $\kappa_i$, (ii) the existential variables used, $W_i$,
and (iii) the
final consequent, $\Delta_i$. The final consequent is used to return a constraint for
$\{\vect{v}\}$ via $\exists {W_i} \cdot \pi_i$. A set of $n$ answers is
returned by the fold step since we allow it to explore multiple ways of
matching up with its disjunctive heap state (as depicted by the entailment's special set residue
 ${ \{ ( {\Delta_i}, \kappa_i, V_i, \pi_i)\}^n_{i=1}}$ --  note that only $\Delta_i$ is an actual heap residue, the rest of the tuple's elements are additional information required to fill up by the {\rulename{ENT-FOLD}} entailment rule).


When a fold against a predicate $\view{p}{u_2,
  \vect{v}_2}$ is performed, the constraints related to variables
$\code{\vect{v}_2}$ are significant. The $\splitKW$ function projects these
constraints out and differentiates those constraints based on free
variables, distinguishing what can be assumed, $\pi_i^a$, from what needs to be proved to hold true, ($\pi_i^c$). The formal definition of $\splitKW$ is as follows:

\begin{center}
  \small
  \renewcommand{\arraystretch}{1.2}
  \begin{tabular}{lll}
    $\splitKW^{\{\code{\vect{v}_2}\}}_V(\bigwedge^n_{i{=}1} \pi^r_i)$
    & $\defeq$
    & $\code{let~} \pi_i^a, \pi^c_i =
    \code{if~} \textit{FV}(\pi_i^r) \cap \code{\vect{v}_2} =
    \emptyset \code{~then~} (\true, \true)$ \\

    &
    & \hspace{5.8em}
    $\code{else~if~} \textit{FV}(\pi_i^r) \cap V = \emptyset
    \code{~then~} (\pi_i^r,\true)$ \\

    &
    & \hspace{5.8em}
    $\code{else~} (\true, \pi_i^r)$ \\

    &
    & $\code{in~} (\bigwedge^n_{i=1} \pi_i^a, \bigwedge^n_{i=1} \pi^c_i)$
  \end{tabular}
\end{center}

In other words, $\splitKW$ helps distinguishing between the pure constraints which are the result of the fold operation introducing 
bindings for the parameters of the folded predicate, from the pure constraints which are in the definition of the folded predicate. The former are transferred to the antecedent, while the latter remain in the consequent since they need to be proven to hold. 


% We demonstrate the \rulename{ENT-FOLD} by the following entailment
% \begin{center}
% \code{\node{node}{x}{1, q_1} * \node{node}{q_1}{2, \nil} * \node{node}{y}{3, \nil}
%   \vdash \view{ll_n}{x, n} \wedge n{>}1 * \St_R}
% \end{center}

% The folding process is applied recursively to the antecedent of the entailment,
% reducing the antecedent until it can match the consequent:

% \begin{tabular}{lll}
%   $\view{ll_n}{x, 2} * \node{node}{y}{3, \nil}$
%   & $\vdash$ & \code{\view{ll_n}{x, n} \wedge n{>}1 * \St_R} \\
% \code{\node{node}{y}{3, \nil} \wedge n{=}2} & $\vdash$ & \code{n{>}1 * \St_R}
% \end{tabular}

\paragraph{\textbf{Approximating a separation formula by a pure formula}}

In our entailment proof, the entailment between separation formulae is
finally reduced to entailment between pure formulae by successively
removing heap nodes from the consequent until only a pure formula remains.
When this happens, the heap formula in the antecedent can be soundly
approximated by the $\XPure_n$ function. The index $\code{n}$ indicates how
precise the caller wants the approximation to be. The function $\XPure_n$
is recursively defined as follows:

\vspace{1em}
\begin{small}
  \renewcommand{\arraystretch}{1.2}
  \begin{tabular}{ll}
    -- &
    $\XPure_n(\emp) \defeq \true$ \\

    --
    & $\XPure_n(\node{u}{d}{\vect{v}}) \defeq \code{u} \neq \nil$ \\

    --
    & $\XPure_n(\view{\pred}{\vect{v}}) \defeq
    \Inv_n(\view{\pred}{\vect{v}})$ \\

    --
    & $\XPure_n(\kappa_1 * \kappa_2) \defeq \XPure_n(\kappa_1) \wedge
    \XPure_n(\kappa_2)$ \\

    --
    & $\XPure_n(\bigvee^n_{i{=}1} (\exists \code{\vect{v}_i} \cdot
    (\kappa_i \wedge \pi_i))) \defeq \bigvee^n_{i{=}1} (\exists
    \code{\vect{v}_i} \cdot (\XPure_n(\kappa_i) \wedge \pi_i))$
  \end{tabular}
\end{small}
\vspace{1em}

In the above definition, $\XPure_n$ calls a related function $\Inv_n$ to
compute the pure approximation. This function $\Inv_n$ is defined as
follows.

\begin{center}
  \small
  \begin{tabular}{c}
    \minipageInvBase
    \hspace{5em}
    \minipageInvRecursive
  \end{tabular}
\end{center}

Specifically, when $\code{n}{=}0$, $\Inv_n$ returns the user-supplied
invariant. In the recursive case, $\code{n}{>}0$, $\Inv_n$ invokes
$\XPure_{n-1}$ to compute a more precise invariant based on the body of the
predicate.




% Now, we
% illustrate how the approximation functions work by computing
% \code{\XPure_1(\view{ll_n}{u,n})}. Let $\Phi$ be the body of the \code{ll}
% predicate, i.e. $\Phi \equiv (\code{u}{=}\nil) \vee \exists \code{r} \cdot
% (\node{u}{node}{\_, r} * \view{ll_n}{r, n-1})$.

% \begin{itemize}
% \item $\Inv_0(\view{ll_n}{u, n}) \equiv \code{n}{\geq}0$

% \item $\XPure_0(\Phi) \equiv (\code{u}{=}\nil \wedge n{=}0) \vee
%   \exists \code{r} \cdot (\code{u}{\neq}\nil \wedge \Inv_0(\view{ll_n}{r, n-1})$\\
%   %
%   \hspace*{4.5em} $\equiv (\code{u}{=}\nil \wedge n{=}0) \vee
%   (\code{u}{\neq}\nil \wedge \code{n}{-}1{\geq}0)$

% \item $\Inv_1(\view{ll_n}{u, n}) \equiv \XPure_0(\Phi) \equiv
%   (\code{u}{=}\nil \wedge \code{n}{=}0) \vee (\code{u}{\neq}\nil \wedge
%   \code{n}{-}1{\geq}0)$

% \item $\XPure_1(\view{ll_n}{u, n}) \equiv \Inv_1(\view{ll_n}{u,n}) \equiv
%   (u{=}\nil \wedge n{=}0) \vee (u{\neq}\nil \wedge n{-}1{\geq}0)$
% \end{itemize}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
