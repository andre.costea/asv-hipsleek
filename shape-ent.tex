The bi-abductive entailment formalism in HIP system is of
the following form
\[
\form{[v_1,..,v_n]~\D_1 \vdash \D_2 \leadsto
(\prestate{p},\D_r,\RA)}
\]
where the left-hand side of \form{\leadsto}
is its input and the right-hand side is the output ($\Delta_r$ is the residua heap).
The procedure infers the output s.t. the following entailment holds:
%
\[ \RA ~{\wedge}~\D_{1} ~{\wedge}~\prestate{p}
\vDash \D_{2} {\sep} \D_r
\]
Three new features are added here to support incremental inference:
\begin{itemize}
\item We may specify a set of 
variables \form{\{v_1,..,v_n\}} for which inference
is to be  applied.
 As a special case, 
when no variables are specified, the entailment system 
reduces to forward verification without any inference capability.
\item We allow {\em second-order variables}, in the form of
{\em uninterpreted} relations, to support incremental inference
for pre/post specifications.
\item We then
collect a set of constraints captured by either \prestate{p} (for
the first-order selective variables) and \form{\RA} (for
the second-order variables)
of the form \form{\form{\RA ~=~
\bigwedge^{n}_{i=1} (\D_{i} \mid \D_g \imply
\D'_{i})}}
to provide interpretations for second-order variables.
\end{itemize}
This approach is critical
for capturing inductive definitions that can
be refined via fix-point
analyses.
~\\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\noindent{\bf Shape Inference. }
There
are two scenarios to consider 
for unknown predicates:
(1) \form{\D_{1}} contains
an {\em unknown}
 predicate instance that matched with
 a 
points-to or
known
predicate
in \form{\D_{2}};
(2) \form{\D_{2}} contains
an {\em unknown} predicate instance.

 An example
of the first scenario is (where
the data structure \code{snode} is defined as
\code{data~snode~\{~snode~next\}}):
%
\[
  \inferS{\code{U}}{\code{\view{\code{U}}{x}}}{\code{\node{x}{snode}{n}}}{\true}
{\code{\view{\code{U}}{x}}{\imply}{\code{\node{x}{snode}{n}}{\sep}\code{\view{U_0}{n}}}}{~\code{\view{U_0}{n}}}
\]
%
Here, we generated a
relational assumption to
denote an {\em unfolding} (or instantiation) for the 
unknown predicate \code{U}
to a heap node \code{snode} followed by another unknown 
\code{U_0(n)} predicate. 
%
An example of the second scenario is shown next.
\[
\begin{array}[t]{l}
  [\code{U_1}]~{\code{\node{x}{snode}{\nil}{\sep}\node{y}{snode}{\nil}}} ~{\vdash}~ {\code{\view{U_1}{x}}}\\
\qquad~{\leadsto}~
({\true},
{\code{\node{x}{snode}{\nil}{\imply}\view{U_1}{x}}},{\code{\node{y}{snode}{\nil}}})
\end{array}
\]
%
The generated 
relational assumption depicts
a {\em folding} process for unknown \code{U_1(x)} 
which captures
a heap state traversed from the pointer \code{x}.
Both folding and unfolding of unknown predicates
are crucial for second-order bi-abduction.

%\noindent{\bf Formalism. }
%{unfold}
Bi-abductive unfold and fold are formalized in Fig.~\ref{fig.sobd.rules}.
For bi-abductive unfold, \form{\FIL{\bar{w}}{\pure}} is an auxiliary function that
existentially quantifies in {\pure} all free variables that are not in the set \form{\bar{w}}. 
\begin{figure}[t]
\[
\begin{array}{c}
\infrule{UNFOLD}\\
\code{\heap_s \equiv \node{r}{c}{\bar{p}}
~\mathsf{or}~ \heap_s \equiv\view{P}{r,\bar{p}}}
\\
\code{\heap_{fields} = \sep_{p_j{\in}\bar{p}}
~\view{\UNK_j}{p_j,\bar{v}_i\NI,\bar{v}_n\NI }~~
\text{where}~\UNK_j \text{:}~
\text{fresh~preds}}
\\
\code{\heap_{rem} = ~\view{\UNK_{rem}}{\bar{v}_i,\bar{v}_n\NI,r\NI}~~
\text{where}~\UNK_{rem} \text{: ~a}~\text{fresh~pred}}
\\
\!\!\code{ \pure_a =
\FIL{\{r,\bar{v}_i,\bar{v}_n,\bar{p}\}}{\pure_1}
\quad \pure_c = \FIL{\{\bar{p}\}}{\pure_2}}
\\
\code{\horn \equiv
(\view{\UNK}{r,\bar{v}_i,\bar{v}_n\NI}
~{\wedge}~\pure_a~{\imply}~ \heap_s~{\sep}
 \heap_{fields}~{*}\heap_{rem}
 ~{\wedge}~\pure_c)}
 \\
\inferS{v^*}{\heap_1~{\sep}~\heap_{fields}~{*}\heap_{rem}~{\wedge}~\pure_1}
{\heap_2 ~{\wedge}~\pure_2}{\RA}{\true}{\D_R}\\
\hline
{\inferS{\code{\UNK},v^*}{
\view{\code{\UNK}}{r,\bar{v}_i,\bar{v}_n\NI}{\sep}\heap_1{\wedge}\pure_1}
{\heap_s{\sep}\heap_2
{\wedge}\pure_2}{\horn{\wedge}\RA}{\true}{\D_R}}
\end{array}
\]
\[
\begin{array}{c}
\infrule{FOLD}\\
\code{ \heap_{11} {=} \code{reach}(\bar{w},\heap_1{\wedge}\pure_1,\bar{z}\NI) \quad ~
\exists \heap_{12} \cdot \heap_1 {=} \heap_{11} {\sep} \heap_{12} }
\\
\code{ \heap_{g} = {\sep}\{ \heap \mid \heap{\in}
\form{\code{\heappred}(\heap_{12}) {\wedge}
\textit{root}(\heap){\subseteq} \bar{z} 
 } \} } ~\quad
\code{ \bar{r} {=} \bigcup_{ \heap{\in}\code{\heap_g }} \textit{root}(\heap) }
\\
\!\!\!\code{\horn~{\equiv}~(\heap_{11} {\wedge}\FIL{\bar{w}}{\pure_1}~{\imply}~
\view{\UNK_c}{\bar{w},\bar{z}\NI}  \guard\heap_{g}{\wedge}\FIL{\bar{r}}{\pure_1}})
\\
\inferS{v^*}{\heap_{12}~{\wedge}~\pure_1}
{\heap_2 ~{\wedge}~\pure_2}{\RA}{\true}{\D_R}\\
\hline
{\inferS{\code{\UNK_c},v^*}{~\heap_1 ~{\wedge}~\pure_1}
{\view{\code{\UNK_c}}{\bar{w},\bar{z}\NI}~{\sep}~\heap_2
~{\wedge}~\pure_2}{\horn {\wedge} \RA}{\true}{\D_R}}
%
\end{array}
\]
\caption{Bi-Abductive Unfolding and Folding.}
\label{fig.sobd.rules}
\end{figure}
%
\noindent
Thus it 
 eliminates from \form{\pure} all subformulas not related to \form{\bar{w}}
({\em e.g.} \sm{\form{\FIL{\{x,q\}}{q{=}\nil {\wedge}y{>}3}}}
returns \sm{\form{q{=}\nil}}).
An RHS assertion is either a points-to
assertion \code{\form{\node{r}{c}{\bar{p}}}}
or a known predicate
instance \code{\form{\view{P}{r,\bar{p}}}}
is paired through the parameter \code{r} with the unknown
 predicate \form{\code{\UNK}}.
Second, the unknown predicates \code{U_{j}} are generated for the data fields of \code{\heap_s}.
Third, the unknown predicate \code{U_{rem}} is generated for the instantiatable parameters
\code{\bar{v}_i} of \form{\code{\UNK}}.
The fourth and fifth lines compute relevant pure formulas and generate the
assumption, respectively.
Finally, the unknown predicates \code{\form{\heap_{fields}}} and \code{\form{\heap_{rem}}}
are combined in the residue of LHS to continue discharging the remaining formula in RHS.


%{fold}
For bi-abductive fold,
the  function  
\sm{\code{reach}(\code{\bar{w},\heap_1{\wedge}\pure_1,\bar{z}\NI)}}
extracts  portions from the antecedent heap (\code{\heap_1})
 that are (1) unknown predicates containing at least one instantiatable parameter
from \code{\bar{w}};
or (2) points-to or known predicates reachable
from   \code{\bar{w}}, but   not reachable
from  \code{\bar{z}}. 
%
%
The $\code{\heappred(\D)}$ function 
enumerates all known predicate instances (of the form \code{\view{P}{\bar{v}}})
and points-to instances (of the form \code{\node{r}{c}{\bar{v}})}) in 
$\code{\D}$.
The  function
 \sm{\textit{root}(\heap)} is defined as:
 \sm{\textit{root} (\code{\node{r}{c}{\bar{v}})}){=}\{\code{r}\}},
\sm{\textit{root} (\code{\view{P}{r,\bar{v}} })=\{\code{r}\}}.
In the first line, heaps of LHS are separated into the assumption
%
 \form{\heap_{11}} and the residue \form{\heap_{12}}.
Second, heap guards (and their root pointers) are inferred based on \form{\heap_{12}}
and the \#-annotated parameters \code{\bar{z}}.
The assumption is generated in the third line and finally,
the residual heap is used to discharge the remaining heaps of RHS.
~\\

\input{shape-infer-pure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\noindent{\bf Pure Inference. }
When both the antecedent and the consequent 
are heap free, the  rules   in Figure~\ref{fig:purerule}
for pure inference can apply. Take note that 
these rules are to be applied in a {\em  top-down} and {\em
left-to-right} order.

\begin{itemize}
\item \rulen{INF-[AND]} repeatedly breaks the conjunctive consequent 
into smaller components.
\item \rulen{INF-[UNSAT]} and \rulen{INF-[VALID]} infer \code{\true} precondition 
whenever the entailment already succeeds. 
Specifically, the rule \rulen{INF-[UNSAT]} applies when the antecedent \sm{\atomic{1}} of the entailment 
is unsatisfiable, whereas the rule 
 \rulen{INF-[VALID]} is used if \rulen{INF-[UNSAT]} cannot be applied, meaning that the 
antecedent is satisfiable.
\item The pure precondition inference is captured by two rules
\rulen{INF-[LHS-CONTRA]} and \rulen{INF-[PRE-DERIVE]}. While 
the first rule handles antecedent contradiction, 
the second one
infers the missing information from the antecedent required for proving the consequent.
Specifically, whenever a contradiction is detected between
the antecedent \code{\atomic{1}} and the consequent \code{\atomic{2}}, 
then the rule \rulen{INF-[LHS-CONTRA]} applies and 
the precondition ${\forall (\FV(\atomic{1}){-}v^*) \cdot \neg \atomic{1}}$
contradicting with the antecedent is being inferred. 
Note that \form{\FV(\cdots)} returns the set of free variables from its argument(s), 
while \form{v^*} is a shorthand notation for \form{v_1,..,v_n}\footnote{If 
there is no ambiguity, we can use $v^*$ instead of $\{v^*\}$.}.
On the other hand, if no contradiction is detected, 
then the rule \rulen{INF-[PRE-DERIVE]} infers a sufficient precondition 
required for proving the consequent.
\item The last two rules \rulen{INF-[REL-DEFN]} and \rulen{INF-[REL-OBLG]} are meant to
gather definitions and obligations, respectively, for the uninterpreted
relation \sm{v_{rel}(u^*)}.
For simplicity, in the rule \rulen{INF-[REL-OBLG]}, we just formalize the case when
there is only one uninterpreted relation in the antecedent.
\end{itemize}

For example, to illustrate how the selective inference works,
consider three entailments below with
\code{\hformn{x}{node}{\_,q}} as a consequent:
\[
\begin{array}[t]{l}
\code{\inferS{n}{\hformp{x}{sllN}{n}}{\hformn{x}{node}
{\_,q}}{\hformp{q}{sllN}{n{-}1}}{n{>}0}{\true}}\\
\code{\inferS{x}{\hformp{x}{sllN}{n}}{\hformn{x}
{node}{\_,q}}{\hformp{q}{sllN}{n{-}1}}{x{\neq}\nil}{\true}}\\
\code{\inferS{n,x}{\hformp{x}{sllN}{n}}{\hformn{x}{node}{\_,q}}
{\hformp{q}{sllN}{n{-}1}}{n{>}0{\vee}x{\neq}\nil}{\true}}
\end{array}
\]
Predicate \form{\hformp{x}{sllN}{n}}
by itself does not 
entail a non-empty node. For the entailment checking to succeed, the current
state would have to be strengthened with either \code{x{\neq}\nil}
or \code{n{>}0}. Our procedure can decide on which pre-condition
to return, depending on the set of variables for
which pre-conditions are to be built from. 
The selectivity is important 
since we only consider a subset of variables (e.g. \code{a,b,r}),
which are introduced to capture pure properties of data structures.
