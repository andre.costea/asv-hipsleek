HIP/SLEEK is written in OCaml \cite{Remy:POPL:1997}, consisting of approximately 260 source files, with approximately
232,000 lines of OCaml code. It is structured in a modular manner to facilitate the addition of new features, allowing
it to become a more fully featured software verifier.

By default, HIP works on a C-like language that allows for the definition of data structures, shape predicates involving
these data structures, and function pre- and post-conditions. In addition, it also has a variety of extended language
parsers that work on common languages like C and Java, which allow the specification of predicates and pre- and
post-conditions inside specially annotated comment blocks. This allows it to work on some real world programs.

SLEEK works with a syntax that closely mimics standard mathematical notation for logic, with some notational
simplifications for use as code (such as \texttt{\&} for $\land$). SLEEK's language also includes similar syntax to HIP
for defining data structures and shape predicates, which allows it to be used independently of HIP as an entailment
prover that can handle user-defined data structures and predicates.

HIP and SLEEK work together in tandem, with HIP generating proof obligations on code that are to be proven by SLEEK.

Entailment in SLEEK proceeds by matching up heap nodes from the antecedent to the consequent, following the rules
described in Section \ref{sec:entailment}. Once the consequent only contains pure formulae, we use $\XPure_n$ to soundly
approximate any remaining heap formula in the antecedent as a pure formula. SLEEK then uses a range of theorem provers
to discharge the now pure entailment. The theorem provers include Z3 \cite{Moura:TACAS:2008}, MONA
\cite{Elgaard:CAV:1998}, Isabelle \cite{Nipkow:Springer:2002}, the Coq proof assistant or the Omega Calculator  constraint solver \cite{Pugh:SC:1991}. Z3 is used by default, with the other provers being enabled by the user as needed. 

\subsection{Proof Search Heuristic}
In contrast to most entailment provers, SLEEK employs a relatively straightforward proof search heuristic when
attempting to prove an entailment. If it sees a data node on the left hand side, and another on the right, with both
having the same name, it will attempt to match both nodes, and remove them from the next entailment step. For a pair of
predicate instances, we will attempt a match, followed by a fold if it does not succeed, followed by an unfold if the fold fails.

For a data node on the left, and a view on the right, we attempt fo fold the data node with other nodes on the left to
try to obtain the same view that we have on the right. In the reverse situation (i.e. view on the left, data on the
right), we unfold the view and attempt to match the new nodes.

If lemmas (\secref{sec:entailment}) are involved, we expand the search to include the applicable lemmas. These lemmas
are applied where possible, and the proof search continues with the entailments that were transformed with the lemmas.

\subsection{Formula Transformation}
Because SLEEK relies on various theorem provers to discharge pure entailments, there is a need to ensure that the
formulae that SLEEK gives to the theorem prover are in the right format of
% understandable by
 the prover; different theorem provers have
different capabilities, and understand different sets of formulae. Thus, other than negotiating the different syntaxes
between SLEEK and the various theorem provers, there is also a need to transform formulae that a theorem prover does not
understand into a semantically equivalent form that it can understand.

For example, the Omega Calculator does not support the formula $z = max(x,y)$ directly. Thus, when we need the Omega
Calculator to verify formulae containing $z = max(x,y)$, we transform it into its logical form, i.e. $z = max(x,y)$
becomes $z = y \land y > x \lor z = x \land x \geq y$.

%This allows
 SLEEK supports a wide range of theorem provers and
% allows
provides options for the users to choose %the best
 theorem provers for the
programs they need to  verify, to achieve the performance they need.
We set to a
combination of Z3 and the Omega Calculator as the default provers. In our experiments, we have found that this gives the best performance in
most cases, taking advantage of the strengths of both provers.

% \subsection{Features}
% HIP/SLEEK also includes features that are unique to HIP/SLEEK. Some of them are detailed here.

% \subsubsection{Lemma Application}
% HIP/SLEEK includes a lemma specification and proving mechanism that

\subsection{Experiments}
Table \ref{tbl:verificationTimes} shows the %% verification times
experimental results
for a suite of test programs %% , which test HIP/SLEEK
over a
variety of data structures. The tests were performed on an Intel\textsuperscript{\textregistered}\
Core\textsuperscript{\texttrademark}\ i7-960, 3.20 GHz. For each example, we note the following information (in order):

\begin{itemize}
  \item The function being verified.
  \item The number of lines of code of the function. This includes the lines of code for all functions that are being
  called by the function being verified.
  \item The number of lines of annotations needed, including shape definitions.
  \item The verification time, when Z3 and the Omega Calculator are used to discharge pure obligations.
  \item The verification time, when MONA is used to discharge pure obligations.
\end{itemize}

We also note the additional properties verified beside each category. The verification times are given in seconds, to 3
significant figures.

The average cost of annotation, i.e. the number of lines of annotations to the number of lines of code, is ~18%.

\begin{table}
  \centering
   \caption{Verification times (in seconds) for data structures with arithmetic and bag/set constraints}
  \begin{tabular}{| l || c | c || c | c || c |}
    \hline
    Program & Lines of & Lines of & \multicolumn{2}{c||}{Verification Time} & Verification Time \\
    \cline{4-6}
    & code & annotation & Z3 + Omega & MONA & MONA \\
    \hline
    \multicolumn{3}{|l||}{\textbf{Linked List}} & \multicolumn{2}{l||}{\textit{size/length}} & \multicolumn{1}{l|}{\textit{bag/set}} \\
    \hline
    delete & 11 & 5 & 0.712 & 1.03 & 1.90 \\
    reverse & 11 & 5 & 0.726 & 1.08 & 2.40 \\
    \hline
    \multicolumn{3}{|l||}{\textbf{Circular List}} & \multicolumn{2}{l||}{\textit{size + cyclic structure}} & \multicolumn{1}{l|}{\textit{bag/set + cyclic structure}} \\
    \hline
    delete (first) & 11 & 5 & 0.568 & 0.723 & 2.53 \\
    count & 24 & 10 & 0.611 & 0.714 & 2.52 \\
    \hline
    \multicolumn{3}{|l||}{\textbf{Doubly-Linked List}} & \multicolumn{2}{l||}{\textit{size + double links}} & \multicolumn{1}{l|}{\textit{bag/set + double links}} \\
    \hline
    append & 20 & 5 & 1.02 & 2.11 & 3.22 \\
    delete & 19 & 5 & 1.28 & 3.45 & 29.69 \\
    \hline
    \multicolumn{3}{|l||}{\textbf{Sorted List}} & \multicolumn{2}{l||}{\textit{size + min + max + sortedness}} & \multicolumn{1}{l|}{\textit{bag/set + sortedness}} \\
    \hline
    delete & 18 & 5 & 0.911 & 20.8 & 3.41 \\
    insertion\_sort & 30 & 10 & 1.01 & failed & 2.58\\
    selection\_sort & 39 & 13 & 0.691 & 5.24 & 2.11 \\
    bubble\_sort & 31 & 17 & 0.533 & 0.556 & 1.34 \\
    merge\_sort & 87 & 17 & 1.09 & failed & 12.1 \\
    quick\_sort & 63 & 17 & 2.82 & failed & 6.69 \\
    \hline
    \multicolumn{3}{|l||}{\textbf{Binary Search Tree}} & \multicolumn{2}{l||}{\textit{min + max + sortedness}} & \multicolumn{1}{l|}{\textit{bag/set + sortedness}} \\
    \hline
    insert & 21 & 5 & 1.31 & 25.0 & 5.20 \\
    delete & 48 & 7 & 1.39 & 20.8 & 10.2 \\
    \hline
    \multicolumn{3}{|l||}{\textbf{AVL Tree}} & \multicolumn{2}{l||}{\textit{size + height}} & \multicolumn{1}{l|}{\textit{bag/set + height}}\\
    \multicolumn{3}{|l||}{} & \multicolumn{2}{l||}{\textit{+ height-balanced}} & \multicolumn{1}{l|}{\textit{+ height-balanced}} \\
    \hline
    insert & 118 & 20 & 31.2 & failed & failed \\
    delete & 129 & 20 & 10.92 & 11.2 & 31.3 \\
    \hline
    \multicolumn{3}{|l||}{\textbf{Red-Black Tree}} & \multicolumn{2}{l||}{\textit{size + height}} & \multicolumn{1}{l|}{\textit{bag/set + height}} \\
    \multicolumn{3}{|l||}{} & \multicolumn{2}{l||}{\textit{+ height-balanced}} & \multicolumn{1}{l|}{\textit{+ height-balanced}} \\
    \hline
    insert & 236 & 50 & 8.71 & failed & 106 \\
    delete & 308 & 50 & 2.89 & 2.97 & 165\\
    \hline
  \end{tabular}
  \label{tbl:verificationTimes}
\end{table}

We give a brief summary of the properties captured in each category:

\begin{itemize}
  \item For singly-linked lists (\textbf{Linked List}), \textbf{circular lists}, and \textbf{doubly-linked lists}, the
  specifications capture the size of the lists (i.e. the total number of nodes). For circular and doubly-linked lists,
  the cyclic structure and the double links, respectively, are captured as well.
  \item For \textbf{sorted lists}, the size of the list, the minimum element, and maximum elements are tracked. The
  sortedness property is expressed using the minimum element. If the specifications contain the bag or set of reachable
  values, the sortedness is expressed directly over the bag or set, with no need to explicitly track the minimum value.
  \item \textbf{Binary search trees} require the tree elements to be sorted. As such, similar to sorted lists, we
  capture this sortedness property by either tracking the minimum/maximum values within the tree, or via the bag or set
  of reachable values.
  \item For \textbf{AVL trees}, we capture the total number of nodes in the tree  (its \textit{size}), and its height.
  Additionally, we specify and invariant that ensures that the tree is \textit{height-balanced}, i.e. that its left and
  right subtrees are nearly balanced. Even when we have the bag or set of reachable values, we continue to track the
  height of the tree, to maintain this invariant.
  \item For \textbf{red-black trees}, we track the size and the \textit{black height} (i.e. the height when considering
  only the black nodes). We also include an invariant that ensures that the tree is height-balanced in its black height,
  meaning that each node's left and right subtree have the same black height.
\end{itemize}

By default, we utilise a combination of Z3 and the Omega Calculator to discharge pure proof obligations, with each
prover covering for the weaknesses of the other. The effectiveness of this combination is seen in the speed at which the
tests complete, with Z3 and the Omega Calculator verifying the examples faster than MONA, and sometimes an order of
magnitude faster.

However, this combination of Z3 and the Omega Calculator is limited in its ability to handle sets and bags of values,
and thus when specifications involve bags or sets, we turn to MONA to discharge these proof obligations. MONA is,
however, also limited in its ability to handle complex formulae. When the formula becomes too complex, MONA is unable to
proceed with discharging the necessary pure proof obligations. This is noted where MONA fails to verify the example.

% MONA fails when the formula is too complex

HIP/SLEEK has also participated in 2 software verification competitions over the years: the Software Verification
Competition, or SV-COMP, and the Separation Logic Competition, or SL-COMP. Here, we present the results from SV-COMP
2016, and SL-COMP 2014.

\begin{table}
  \centering
    \caption{SV-COMP 2016 Results}
  \begin{tabular}{|l|r|r|r|r|r|r|}
    \hline
    \thead{Category} & \thead{Total Tests} & \thead{Correct} & \thead{Incorrect} & \thead{Time Taken (s)} & \thead{Max Score} & \thead{Score Obtained} \\
    \hline
    Recursive & 98 & 70 & 0 & 9700 & 151 & 111 \\
    \hline
  \end{tabular}
  \label{tbl:svcomp2016}
\end{table}

In SV-COMP 2016, HIP participated in the Recursive category, which tests the ability of software verifiers to do
recursive analysis on programs \footnote{\url{\texttt{https://sv-comp.sosy-lab.org/2016/benchmarks.php}}}. HIP's
performance in SV-COMP 2016 is summarised in Table \ref{tbl:svcomp2016}.

\begin{table}
  \centering
    \caption{SL-COMP 2014 Results}
  \begin{tabular}{|l|r|r|r|r|}
    \hline
    \thead{Category} & \thead{Total Tests} & \thead{Correct} & \thead{Incorrect} & \thead{Time Taken (s)} \\
    \hline
    \texttt{sll}$(\vDash)$ & 110 & 110 & 0 & 4.99 \\
    \texttt{sll}$(\Rightarrow)$ & 292 & 292 & 0 & 14.13 \\
    \texttt{FDB}$(\Rightarrow)$ & 43 & 31 & 1 & 43.65 \\
    \texttt{UDB}$(\vDash)$ & 61 & 61 & 0 & 30.84 \\
    \texttt{UDB}$(\Rightarrow)$ & 172 & 131 & 4 & 80.60 \\
    \hline
  \end{tabular}
  \label{tbl:slcomp2014}
\end{table}

SLEEK also participated in SL-COMP 2014. The results are summarised in Table \ref{tbl:slcomp2014}. The individual
categories are\footnote{from \texttt{\url{https://github.com/sl-comp/SL-COMP14/tree/master/bench}}}:

\begin{itemize}
  \item \texttt{sll}$(\vDash)$: satisfiability problems for symbolic heaps with list segment predicates,
  \item \texttt{sll}$(\Rightarrow)$: entailment problems for symbolic heaps with list segment predicates,
  \item \texttt{FDB}$(\Rightarrow)$: entailment problems for symbolic heaps with composed lists,
  \item \texttt{UDB}$(\vDash)$: satisfiability problems for symbolic heaps with inductive definitions,
  \item \texttt{UDB}$(\Rightarrow)$: entailment problems for symbolic heaps with inductive definitions.
\end{itemize}


HIP/SLEEK can be accessed as an open source tool from \cite{HIPgit} or through its web interface at \cite{HIPonline}.