\begin{figure}[tb]
\centering
	\lstset{
		numbers=left,
		numberfirstline=true,
		numberblanklines=false
	}
	\begin{lstlisting}[mathescape=true,firstnumber=1]{language=customc}
data node {node prev; node next; }

void sll2dll(node x, node q)
{ // ${S_1:~ \view{H}{x,q\NI}}$
   if(x!=NULL) { 
   	// $ S_2:~ \node{x}{node}{x_p,x_n} {\sep} \view{H_p}{x_p,q\NI} {\sep}\view{H_n}{x_n,q\NI} {\wedge}\code{x{\ne}\nil} $
   	x.prev=q;
   	// $S_3:~ \node{x}{node}{\textcolor{black}{q},x_n} {\sep} \view{H_p}{x_p,q\NI} {\sep}\view{H_n}{x_n,q\NI} {\wedge}\code{x{\ne}\nil}$ 
        sll2dll(x.next,x);
        //$S_4:~ \node{x}{node}{q,x_n} {\sep} \view{H_p}{x_p,q\NI} {\sep}\textcolor{black}{\view{G}{x_n,x\NI}} {\wedge}\code{x{\ne}\nil}$
  }
 //$S_5:~\view{H}{x,q\NI}{\wedge}\code{x{=}\nil}$
}
	\end{lstlisting}
	\caption{Example}
	\label{shape.an.ex}
\end{figure}

As a running example, consider the method shown in
Figure \ref{shape.an.ex} which
traverses a singly-linked list, and gradually changes the
input list into a doubly-linked list.


To capture the specification that ensures the memory safety
for this method, we could use the following inductive predicates.
\[
\begin{array}{ll}
	\view{sllN}{hd,n} &\equiv \code{\emp {\wedge} hd}{=}\nil \wedge n{=}0\vee
         \exists nx. ~\node{hd}{node}{\_,nx} \sep
         \view{sllN}{nx, n{-}1}\\
	\view{dllN}{hd,p,n} &\equiv \code{\emp {\wedge} hd}{=}\nil \wedge n{=}0 \vee
         \exists nx.~\node{hd}{node}{p,nx}\sep
         \view{dllN}{nx,hd,n{-}1}
\end{array}
\]
Here, the \code{sllN} predicate describes the shape of an acyclic singly-linked list
pointed by \code{hd}.
In its definition, the first disjunct corresponds to the case of an empty list, 
whereas the second one separates a list into two parts:
the head \code{\hformn{hd}{node}{\anon,nx}}, and 
the tail \code{\view{sllN}{nx, n{-}1}}. 
The use of the \code{*} (separating conjunction) 
operator guarantees that these two parts reside 
in disjoint memory regions.
Similarly, the \code{dllN} predicate describes the shape of an acyclic doubly-linked list
pointed by \code{hd}.
With these predicates, we can write
a specification that ensures
memory safety for this method
through the following pair of
pre/post conditions.
\[
\requires ~~ \view{sllN}{x,n} \qquad \quad
\ensures ~~~
\view{dllN}{x,q,n} \wedge n{\geq}0
\]
\hide{
\begin{verbatim}
 requires x::slln<n>
 ensures  x::dlln<p,n>;
\end{verbatim}}

Once predicate definitions
and pre/post specifications for the method
are given, the
automated verification system (as presented in the preceding sections) should be able to
verify that all memory accesses are safe, and that
the post-condition of the method 
is ensured,
namely that a doubly-linked list with the same size with the list in the pre-condition, has been created.
Our shape inference framework
endeavours to undertake the reverse scenario
where predicate definitions are
not given a priori. %% apriori.
 Instead, to trigger the incremental inference in the HIP system,
the end user only needs to annotate
the following command:
\[
\begin{array}{l}
\infer \quad [@shape,@size,@pre,@post] \\
\requires \quad \true \qquad\quad
\ensures \quad \true
\end{array}
\]
where the parameter of the \form{\infer} command describes
all stages which are to be considered during the inference: \form{@shape}
for shape inference, \form{@size} for the size property extension, and
\form{@pre} (respectively, \form{@post}) for the pure constraint in the pre-condition (respectively, post-condition) inference.
%
In the following,
we  illustrate how the HIP system could infer
the above specification via
second-order bi-abduction \cite{Loc:CAV:2014,Trinh:APLAS:2013}.

{\bf Shape Inference. }
The shape inference in HIP
 relies on capabilities of shape analysis. Given
a program, the shape analysis infers shapes of dynamic linked data structures pointer by pointer at program locations that
are required for memory safety.
Shape analysis mechanisms typically infer specifications for memory
safety with a predetermined set of shape predicates \cite{Berdine:CAV11,Calcagno:POPL:09,POPL08:Chang,Magill:POPL10}. However,
discovering arbitrary shape abstractions can be rather challenging, as linked data
structures span a wide variety of forms, from singly-linked lists, doubly-linked
lists, circular lists, to tree-like data structures. Furthermore, such abstractions
would also need to cater to various specializations, such as strictly non-empty
structures or segmented structures (e.g. list/tree segments) with outward
pointing references. It is interesting and challenging to develop a mechanism
that would be capable of inferring complicated shape
specifications, from scratch, directly from the heap-manipulating programs. We now show how this can be done. % in this section.



By command \form{\infer ~ [@shape]}, HIP internally introduces two unknown
predicates, \code{H}   and \code{G},
as the place-holders
to capture the shape specification for  
the pre-condition and the post-condition, respectively, in the following
 specification. % with these predicates isshown as below.
\[
\begin{array}{l}
\infer \quad [H,G] \\
\requires \quad \view{H}{x,q\NI} \qquad\quad
\ensures \quad\view{G}{x,q\NI}
\end{array}
\]
\hide{
\begin{verbatim}
 requires H(x,p)
 ensures  G(x,p);
\end{verbatim}}
 Intuitively, it is meant to incorporate the inference
capability (via \code{infer}) into a pair
of pre/post-conditions (via \code{\requires}/\code{\ensures}).
Here the inference will be applied to second-order variables \code{H,G}.
For clarity, we use $\btt{H_1},\btt{H_2},\ldots$
as names for unknown pre-predicates 
%% extended from other pre-predicates,
and $\btt{G_1},\btt{G_2},\ldots$
as names for unknown post-predicates.


The three steps of our shape inference are:
%\begin{enumerate}
%\item
(i) Derive relational assumptions during code verification
using second-order bi-abduction;
%\item
(ii) Derive predicate definitions
from relational assumptions by abductive 
or equivalence-preserving transformation;
%\item
and (iii) Normalize and, whenever possible, substitute available
predicates for unknowns to support reuse.
%\end{enumerate}
%
Firstly, we apply code verification using these pre/post
specification and attempt to
infer program states \form{S_1,..,S_5} (of the form of separation logic
formulas as shown in Figure \ref{shape.an.ex}) as well as to
collect a set of relational assumptions (over the unknown predicates)
that must hold to ensure memory-safety. These assumptions would also
ensure that the pre-conditions of each method call are satisfied,
and that the post-condition is ensured
at the end of its method body.

For the \code{sll2dll} method, HIP can derive the following four
relational assumptions:
%\wnnay{optimize layout later}
%
\[
\begin{array}{lcl}
 \view{H}{x,q\NI}\wedge \code{x{\ne}\nil} & \imply &
 \node{x}{node}{x_p,x_n} {\sep}
 \view{H_p}{x_p,q\NI}
{\sep}\view{H_n}{x_n,q\NI}
%% \\\qquad\qquad{\sep}\view{HP_3}{p,x}
\\
 %
 \view{H_n}{x_n,q\NI} ~|~ \node{x}{node}{q,x_n} &\imply&
 \view{H}{x_n,x\NI} \\
%
 \view{H}{x,q\NI}\wedge\code{x{=}\nil} & \imply &
\view{G}{x{,}q\NI} \\
 %
 \node{x}{node}{q{,}x_n} {\sep}
 \view{G}{x_n{,}x\NI}
%% {\sep}\view{HP_3}{p,x} 
&\imply &
\view{G}{x{,}q\NI}%\\[3pt]
\end{array}
\]
The first assumption is generated by the access to the \code{x.prev}
field, which mandated that \code{\view{H}{x,q\NI}} 
generates a node under a condition \code{x{\neq}\nil},
from the \code{then} branch of its
conditional statement. 
The second assumption is for modularly
proving the precondition of the recursive call to \code{sll2dll(x.next,x)}.
The last two assumptions are required to ensure
the ascribed post-condition of the \code{sll2dll} method hold for both branches (the else branch is implicit).

There are several new features that we have
designed into our relational
assumptions. Firstly, we may introduce new 
unknown predicates, such as $\btt{H_p}$ and $\btt{H_n}$,
%% $\btt{HP_3}$ 
so as to provide possible expansion
(or instantiation) points for yet to be explored data fields.
%
Secondly, we use a
\NI-annotation scheme
to carefully ensure
that each pointer is
instantiated at most once.
This is to prevent us from accidentally creating
an unsatisfiable \code{\false} state when a pointer
is instantiated more than once.
Our annotation
scheme is fully automatic as we have developed a simple
static analysis to determine the \NI-annotation
for the initial pre/post predicates. In the case
of the \code{sll2dll} example, we will mark the second
parameter of both predicates \code{H} and \code{G} as non-instantiating, i.e.
\code{\view{H}{x,q\NI}} and \code{\view{G}{x,q\NI}},
since it is never field-accessed in
the code. 
%
Thirdly, we allow a special
heap guard introduced after $|$ (e.g. \code{\node{x}{node}{q,x_n}} in the
second assumption),
to clearly describe the heap context where the relational
assumption was applicable. 
This heap guard is used to guide the instantiation of some
parameters (e.g. \code{x\NI}), and is critical for
properly handling back-pointers (such as previous 
or parent pointers).

Based on this set of relational assumptions, it is possible
to construct an interpretation for the unknown $\btt{H}$, $\btt{G}$,
$\btt{H_n}$  predicates that ensure these
assumptions.
As for \hide{the other two unknown
predicates}
$\btt{H_p}$,
%% ,$\btt{HP_3}$, 
we would
refrain from imposing any interpretation \hide{on
them} since its contents are not being
accessed by the \code{sll2dll} method.
%% Hence, no constraints
%% have been imposed on them. 
We refer to predicates without
any interpretation as {\em
dangling predicates} since they denote dangling references that
are not being accessed by the current method.

Our next phase uses a predicate derivation
procedure to 
transform (by either equivalence-preserving or
abductive steps) each set of relational assumptions into  
its corresponding set of predicate definitions.
For our running example, we can generate the following
three predicate definitions, after replacing each
dangling predicate, e.g. $\view{H_p}{x_p,q}$,
that came from expanding a field by a unique
global variable (e.g.
$\DP_{\code{p}}$) which denotes the set of
(unconstrained) references from its (e.g. \code{prev}) field.
%% The dangling predicates of fields that are located
%% %% non-recursively 
%% in the pre-condition can be used
%% to provide 
%% linkages to its corresponding post-condition.
For this example, the dangling reference is captured
recursively within \code{\view{H}{x,q}}, and
 denotes multiple \code{prev} fields of
the singly-linked list that were overwritten
in the post-condition.
The definition of \code{H_n} is in a special
guarded form to facilitate the instantiation
of back-pointers. Instances of such guarded
definitions can always be inlined, so that they
are never required in our final specifications.
(In both assumption
\form{\code{\D_{lhs}{\imply}\D_{rhs}}} and
definition \form{\code{\D_{lhs}{\implyeq}\D_{rhs}}},
the free variables from \form{\code{\FV({\D_{rhs}}){-}\FV({\D_{lhs}}})}
are implicitly existentially quantified.)
\[
	\begin{array}{l}
	\view{H}{x,q} ~\equiv~ \code{\emp {\wedge} x}{=}\nil\vee
	\node{x}{node}{\DP_p,x_n} 
	{\sep}\view{H}{x_n,x} \\
	\view{H_n}{x_n,q}~|~\node{x}{node}{q,x_n} ~\equiv~ \view{H}{x_n,x}\\
	\view{G}{x,q} ~\equiv~ \code{\emp {\wedge} x}{=}\nil\vee
	\node{x}{node}{q,x_n} \sep \view{G}{x_n,x}
	\end{array}	
\]
After deriving all predicate definitions,
we proceed with the last phase on 
normalization to simplify and reuse
predicates, where possible \cite{Loc:CAV:2014,Loc:TACAS:2018}. This phase would
eliminate the second parameter of $\btt{H}$
and the unused
predicate, \code{\view{H_n}{x,p}},
yielding:
\[
	\begin{array}{lcl}
	\view{H}{x,q} &\equiv& \view{H_2}{x}\\
	\view{H_2}{x} &\equiv& \emp {\wedge} \code{x}{=}\nil \vee
	\node{x}{node}{\DP_{\code{p}},x_n} \sep
	\view{H_2}{x_n} \\
	\view{G}{x,q} &\equiv& \emp {\wedge} \code{x}{=}\nil\vee
	\node{x}{node}{q,x_n} \sep \view{G}{x_n,x}
	\end{array}
\]


Note that \code{H_2(x)} is a specialized version
of \code{H(x,q)} without the redundant \code{q} parameter.
Whenever possible, we would also attempt to reuse existing
predicate definitions to support shorter specifications
and improve analysis/verification.
If the following predicates \code{sll} and \code{dll} definitions have been
supplied earlier,
\[
\begin{array}{ll}
	\view{sll}{hd} &\equiv \emp {\wedge} \code{hd}{=}\nil \vee
         \exists nx. ~\node{hd}{node}{\_,nx} \sep
         \view{sll}{nx}\\
	\view{dll}{hd,p} &\equiv \emp {\wedge} \code{hd}{=}\nil \vee
         \exists nx.~\node{hd}{node}{p,nx}\sep
         \view{dll}{nx,hd}
\end{array}
\]
 we would relate our new definitions
to these prior definitions, as follows.
\[
\begin{array}{l}
   \view{H_2}{x} \equiv \view{sll}{x}
   \qquad\qquad
   \view{G}{x,q} \equiv \view{dll}{x,q}
\end{array}
\]

